/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package webxmlaccesser;

import dataObjects.ServerLoginData;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.SwingWorker;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPReply;

/**
 *
 * @author koutsch
 */
public class FTPConnector extends FTPClient {

    /**
     * Create a new FTP connector
     *
     * @throws IOException
     */
    public FTPConnector() throws IOException {
        super();
    }

    /**
     * Log the server reply strings
     */
    private void serverReplyToLog() {
        String[] replies = getReplyStrings();
        if (replies != null && replies.length > 0) {
            for (String aReply : replies) {
                Logger.getLogger(WebXMLAccesser.class.getName())
                        .log(Level.INFO, "Server reply code: {0}", aReply);
            }
        }
    }

    /**
     * Connect and login to a FTP server
     *
     * @param loginData a ServerLoginData object holding the login data
     * @return true if connected, false otherwise
     * @throws IOException
     */
    public boolean connectAndLogin(ServerLoginData loginData) throws IOException {

        // set that after 30000 milliseconds stop if could not connect 
        setConnectTimeout(30000);
        // check if the port number is not -1 and if yes connect without portnumber
        if (loginData.getPort() == -1) {
            connect(loginData.getIpAddress());
        } else {
            connect(loginData.getIpAddress(), loginData.getPort());
        }
        // retrieve the server reply code
        int replyCode = getReplyCode();
        // if the connection was unsuccessful, return false and log reply code
        if (!FTPReply.isPositiveCompletion(replyCode)) {
            Logger.getLogger(WebXMLAccesser.class.getName())
                    .log(Level.INFO, "Connection failed. Server reply code: {0}", replyCode);
            return false;
        }
        // if successful login to the server
        boolean success = login(loginData.getUsername(), loginData.getPassword());
        // log server response
        serverReplyToLog();
        // log and return if successful login
        if (!success) {
            Logger.getLogger(WebXMLAccesser.class.getName())
                    .log(Level.INFO, "Could not log in.", "Login");
            return false;
        } else {
            Logger.getLogger(WebXMLAccesser.class.getName())
                    .log(Level.INFO, "Logged in.", "Login");
            return true;
        }
    }

    /**
     * A SwingWorker handling a download in the background
     */
    public class DownloadWorker extends SwingWorker<File, Void> {

        private final String remoteFileName;
        private final String localFileName;
        private final long remoteFileSize;
        private final ServerLoginData loginData;

        /**
         * Create a new download worker
         *
         * @param remoteFileName the name of the remote file
         * @param localFileName the name of the local file
         * @param loginData the login data
         * @param remoteFileSize the size of the remote file in bytes
         */
        public DownloadWorker(String remoteFileName, String localFileName,
                ServerLoginData loginData, long remoteFileSize) {
            this.remoteFileName = remoteFileName;
            this.localFileName = localFileName;
            this.loginData = loginData;
            this.remoteFileSize = remoteFileSize;
        }

        // the logic
        @Override
        protected File doInBackground() throws IOException, InterruptedException {
            // set progress to zero
            setProgress(0);
            // try connecting
            boolean connectAndLogin = connectAndLogin(loginData);
            // if connected
            if (connectAndLogin) {
                failIfInterrupted();

                // prepare for download communication
                enterLocalPassiveMode();
                setFileType(BINARY_FILE_TYPE, BINARY_FILE_TYPE);
                setFileTransferMode(BINARY_FILE_TYPE);

                // download the file in 4KB chunks
                try (InputStream inputStream = retrieveFileStream(remoteFileName);
                        OutputStream outputStream = new BufferedOutputStream(new FileOutputStream(localFileName))) {
                    byte[] bytesArray = new byte[4096];
                    double progress = 0;
                    int bytesRead;

                    while ((bytesRead = inputStream.read(bytesArray)) != -1) {
                        failIfInterrupted();
                        outputStream.write(bytesArray, 0, bytesRead);
                        progress += bytesRead;
                        setProgress(Math.round((float) ((progress / remoteFileSize) * 100)));
                        Logger.getLogger(WebXMLAccesser.class.getName())
                                .log(Level.INFO, "Downloaded {0} Bytes", progress);
                    }

                    outputStream.close();
                    inputStream.close();

                    failIfInterrupted();
                    completePendingCommand();
                }
                Logger.getLogger(WebXMLAccesser.class.getName())
                        .log(Level.INFO, "Downloaded file {0}", localFileName);
                // return the downloaded file
                return new File(localFileName);
            }
            // return null if no file donloaded
            return null;
        }

        private void failIfInterrupted() throws InterruptedException, IOException {
            if (Thread.currentThread().isInterrupted()) {
                disconnect();
                throw new InterruptedException("Interrupted during execution");
            }
        }
    }

    /**
     * A SwingWorker handling an upload in the background
     */
    public class UploadWorker extends SwingWorker<Boolean, Void> {

        String remoteDirName;
        File[] localFiles;
        private final ServerLoginData loginData;

        /**
         * Create a new UploadWorker
         *
         * @param remoteDirName the name of the remote file
         * @param localFiles the local files
         * @param loginData the login data
         */
        public UploadWorker(String remoteDirName, File[] localFiles, ServerLoginData loginData) {
            this.remoteDirName = remoteDirName;
            this.localFiles = localFiles;
            this.loginData = loginData;
        }

        // the logic
        @Override
        protected Boolean doInBackground() throws IOException, InterruptedException {
            // set the progress to zero
            setProgress(0);
            // try connecting
            boolean connectAndLogin = connectAndLogin(loginData);
            // if connected
            if (connectAndLogin) {

                failIfInterrupted();

                // prepare for download communication
                enterLocalPassiveMode();
                setFileType(BINARY_FILE_TYPE, BINARY_FILE_TYPE);
                setFileType(BINARY_FILE_TYPE);
                // as long as there are files
                for (File localFile : localFiles) {
                    // make an upload
                    OutputStream outputStream;
                    try (FileInputStream inputStream = new FileInputStream(localFile)) {
                        outputStream = storeFileStream(remoteDirName + "/" + localFile.getName());
                        byte[] bytesIn = new byte[4096];
                        double progress = 0;
                        int bytesRead;
                        while ((bytesRead = inputStream.read(bytesIn)) != -1) {
                            failIfInterrupted();
                            outputStream.write(bytesIn, 0, bytesRead);
                            progress += bytesRead;
                            setProgress(Math.round((float) ((progress / localFile.length()) * 100)));
                        }

                        failIfInterrupted();
                        outputStream.close();
                        completePendingCommand();
                    }
                    Logger.getLogger(WebXMLAccesser.class.getName())
                            .log(Level.INFO, "Uploaded file {0}", localFile.getName());
                }

                disconnect();
                // return true if uploaded
                return true;
            }
            // other wise return false
            return false;
        }

        private void failIfInterrupted() throws InterruptedException, IOException {
            if (Thread.currentThread().isInterrupted()) {
                disconnect();
                throw new InterruptedException("Interrupted during execution");
            }
        }
    }
}
