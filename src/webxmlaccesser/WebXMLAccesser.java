/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package webxmlaccesser;

import gui.MainFrame;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

/**
 *
 * @author koutsch
 */
public class WebXMLAccesser {

    static final Logger LOGGER = Logger.getLogger(WebXMLAccesser.class.getName());

    public static String getHomeDirectory() {

        return System.getProperty("user.home");
    }

    /**
     * @param args the command line arguments
     * @throws java.io.IOException
     */
    public static void main(String[] args) throws IOException {

        FileHandler fh;
        System.out.println("Home directory: " + getHomeDirectory());
        ClassLoader cl = ClassLoader.getSystemClassLoader();

        URL[] urls = ((URLClassLoader) cl).getURLs();

        for (URL url : urls) {
            System.out.println(url.getFile());
        }
        fh = new FileHandler(getHomeDirectory()
                + File.separator + "WebXMLAccesserLOG.log");

        LOGGER.addHandler(fh);
//        LOGGER.setUseParentHandlers(false);
        SimpleFormatter formatter = new SimpleFormatter();
        fh.setFormatter(formatter);

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(() -> {
            try {
                // Set System L&F
                UIManager.setLookAndFeel(
                        UIManager.getSystemLookAndFeelClassName());
            } catch (UnsupportedLookAndFeelException | ClassNotFoundException |
                    InstantiationException | IllegalAccessException ex) {
                Logger.getLogger(WebXMLAccesser.class.getName()).log(Level.SEVERE, null, ex);
            }
            new MainFrame().setVisible(true);
        });
    }
}
