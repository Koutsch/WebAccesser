/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package webxmlaccesser;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.apache.xerces.xs.XSAnnotation;
import org.apache.xerces.xs.XSObjectList;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 * Base class for XML and XSD reader
 *
 * @author koutsch
 */
abstract public class XReader {

    /**
     * the XML or XSD Document object
     */
    protected Document doc;
    private final String xmlFormat = "{http://xml.apache.org/xslt}indent-amount";

    /**
     * Get the XML Document instance
     *
     * @return the Document instance or null if not loaded
     */
    public Document getDoc() {
        return doc;
    }

    /**
     * Create a new Document instance
     *
     * @return a new Document object
     * @throws ParserConfigurationException
     */
    public final Document createDocument() throws ParserConfigurationException {

        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        return dBuilder.newDocument();
    }

    /**
     * Transforms the XML Document held by the reader to a String and prints it.
     *
     * @param document XML document to be printed, if null the Document object
     * field is printed
     * @param fileName a String holding the file path, if null the XML is
     * printed to standard output.
     * @throws TransformerConfigurationException
     * @throws TransformerException
     * @throws IOException
     */
    public void printDocument(Document document, String fileName) throws TransformerConfigurationException, TransformerException, IOException {

        if (document == null) {
            document = doc;
        }

        Transformer transformer = TransformerFactory.newInstance().newTransformer();
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        transformer.setOutputProperty(xmlFormat, "5");
        transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
        DOMSource source = new DOMSource(document);
        
        if (fileName != null) {
            Writer out = new OutputStreamWriter(new FileOutputStream(fileName), "UTF8");
            transformer.transform(source, new StreamResult(out));
        } else {
            StreamResult result = new StreamResult(new StringWriter());
            transformer.transform(source, result);
            String xmlString = result.getWriter().toString();
            System.out.println(xmlString);
        }
    }

    /**
     * Gets Element child nodes or child nodes with the name specified
     *
     * @param node the parent node object
     * @param tagName the name of the node tag to be found, if null all children
     * Element nodes are returned
     * @return an ArrayList holding the Elements found, if no child elements an empty ArrayList
     */
    public ArrayList<Element> getChildElements(Node node, String tagName) {

        ArrayList<Element> elementNodes = new ArrayList<>();
        if (node.hasChildNodes()) {
            NodeList childNodes = node.getChildNodes();

            boolean checkTag = (tagName != null);
            for (int i = 0; i < childNodes.getLength(); i++) {
                Node childNode = childNodes.item(i);

                if (childNode.getNodeType() == Node.ELEMENT_NODE) {

                    if (checkTag && childNode.getNodeName().equals(tagName)) {
                        elementNodes.add(((Element) childNode));
                    }

                    if (!checkTag) {
                        elementNodes.add(((Element) childNode));
                    }
                }
            }
        }
        return elementNodes;
    }

    /**
     * Transform XSD annotations to Strings
     *
     * @param annotations a XSObjectList holding XSAnnotation objects
     * @return an ArrayList holding the annotations found as Strings or an empty
     * ArrayList if no annotations found
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws IOException
     */
    public ArrayList<String> getAnnotations(XSObjectList annotations) throws ParserConfigurationException, SAXException, IOException {

        ArrayList<String> annotationStrings = new ArrayList<>();
        if (annotations.getLength() > 0) {
            XSAnnotation xsa = (XSAnnotation) annotations.item(0);
            String annotationString = xsa.getAnnotationString();
            if (annotationString != null && !"".equals(annotationString)) {
                Document annDoc = DocumentBuilderFactory.newInstance().newDocumentBuilder()
                        .parse(new InputSource(new StringReader(annotationString)));
                Node annontation = annDoc.getFirstChild();
                if (annontation != null) {
                    ArrayList<Element> items = getChildElements(annontation, null);
                    for (Element item : items) {
                        annotationStrings.add(item.getFirstChild().getTextContent());
                    }
                }
            }
        }
        return annotationStrings;
    }
}
