/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package webxmlaccesser;

import dataObjects.AddableElement;
import dataObjects.AddedElement;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Source;
import javax.xml.transform.TransformerException;
import javax.xml.transform.dom.DOMSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import org.apache.xerces.dom.AttrImpl;
import org.apache.xerces.xni.parser.InformativeSAXParseException;
import org.apache.xerces.xs.XSAttributeUse;
import org.apache.xerces.xs.XSConstants;
import org.apache.xerces.xs.XSElementDeclaration;
import org.apache.xerces.xs.XSObject;
import org.apache.xerces.xs.XSObjectList;
import org.apache.xerces.xs.XSParticle;
import org.apache.xerces.xs.XSSimpleTypeDefinition;
import org.apache.xerces.xs.XSTypeDefinition;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

/**
 *
 * @author koutsch
 */
public class XMLReader extends XReader {

    private Node current;
    private Node rootNode;
    private XSDReader xsdReader;
    private final List<String> errors = new ArrayList<>();
    private final String getCurrentElement = "http://apache.org/xml/properties/dom/current-element-node";
    private final String schemaLanguage = XMLConstants.W3C_XML_SCHEMA_NS_URI;

    private XMLReader() {
    }

    /**
     * Create a new XMLReader with a XML Schema from file
     *
     * @param xsdFile the corresponding XML Schema file
     * @param xmlDoc a Document representing the XML
     * @return a new XMLReader with both XML and XSD loaded
     * @throws ClassNotFoundException
     * @throws InstantiationException
     * @throws IllegalAccessException
     * @throws SAXException
     * @throws IOException
     * @throws ParserConfigurationException
     */
    public static XMLReader instantiateFromFile(File xsdFile, Document xmlDoc) throws ClassNotFoundException, InstantiationException, IllegalAccessException, SAXException, IOException, ParserConfigurationException {
        XMLReader xmlReader = new XMLReader();
        xmlReader.setXSDReader(XSDReader.instantiate(xsdFile));
        boolean loaded = xmlReader.load(xmlDoc);
        if (loaded) {
            return xmlReader;
        } else {
            return null;
        }
    }

    /**
     * Create a new XMLReader with a XML Schema from an XSDReader object
     *
     * @param xsdReader the corresponding XSDReader object
     * @param xmlDoc a Document representing the XML
     * @return a new XMLReader with both XML and XSD loaded
     */
    public static XMLReader instantiateFromObject(XSDReader xsdReader, Document xmlDoc) {
        XMLReader xmlReader = new XMLReader();
        xmlReader.setXSDReader(xsdReader);
        boolean loaded = xmlReader.load(xmlDoc);
        if (loaded) {
            return xmlReader;
        } else {
            return null;
        }
    }

    /**
     * Set the XSDReader to be used
     *
     * @param xsdReader a valid XSDReader object
     */
    private void setXSDReader(XSDReader xsdReader) {
        this.xsdReader = xsdReader;
    }

    /**
     * Load the Document of this XMLReader
     *
     * @param xmlDoc a Document representing the XML
     * @return true if loaded, otherwise false
     * @throws ClassNotFoundException
     * @throws InstantiationException
     * @throws IllegalAccessException
     * @throws SAXException
     * @throws IOException
     * @throws ParserConfigurationException
     */
    private boolean load(Document xmlDoc) {

        if (xsdReader != null) {
            this.doc = xmlDoc;
            rootNode = current = doc.getFirstChild();
            return true;
        }
        return false;
    }

    /**
     * Create a new Document object of a XML file
     *
     * @param file the XML file
     * @return a Document representing the XML
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws IOException
     */
    public static Document makeXMLDoc(File file) throws ParserConfigurationException, SAXException, IOException {
        DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
        docFactory.setNamespaceAware(true);
        DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
        return docBuilder.parse(file);
    }

    /**
     * Get the current Node
     *
     * @return a Node representing either an Element or an Attribute object
     */
    public Node getCurrent() {
        return current;
    }

    /**
     * Set the current Node
     *
     * @param node the Node object to be declared current
     */
    public void setCurrent(Node node) {
        current = node;
    }

    /**
     * Get the root element
     *
     * @return a Node representing the root element
     */
    public Node getRootNode() {
        return rootNode;
    }

    /**
     * Error Handler receiving element data on validation errors and sorting out
     * the corresponding error messages when check single nodes
     */
    private class ValidationErrorHandler implements ErrorHandler {

        private final Validator validator;
        private final Node node;

        /**
         * Create a new ValidationErrorHandler instance
         *
         * @param validator the validator used for XML validation
         * @param node the node to be checked, all error messages are given
         */
        public ValidationErrorHandler(Validator validator, Node node) {
            this.validator = validator;
            this.node = node;
        }

        @Override
        public void warning(SAXParseException exception) throws SAXException {

            InformativeSAXParseException ispe = (InformativeSAXParseException) exception;
            if (node == null) {

                String msg = "";
                List<String> elementLineage = ispe.getElementLineage();
                if (elementLineage.size() > 0) {
                    msg = elementLineage.stream().map((elementName)
                            -> elementName + " > ").reduce(msg, String::concat);
                }

                String attrName = ispe.getAttrName();
                if (attrName != null) {
                    msg += attrName + " > ";
                }

                msg += ispe.getMessage();
                errors.add(msg);
            } else {
                Node vNode = (Node) validator.getProperty(getCurrentElement);
                if (node.getNodeType() == Node.ELEMENT_NODE && node.equals(vNode)) {
                    errors.add(ispe.getMessage());
                }

                if (node.getNodeType() == Node.ATTRIBUTE_NODE && ((AttrImpl) node).getOwnerElement().equals(vNode)
                        && ispe.getAttrName().equals(((AttrImpl) node).getName())) {
                    errors.add(ispe.getMessage());
                }
            }
        }

        @Override
        public void error(SAXParseException exception) throws SAXException {
            warning(exception);
        }

        @Override
        public void fatalError(SAXParseException exception) throws SAXException {
            warning(exception);
        }

    }

    /**
     * Perform a validation of a XML Document or sub node.
     *
     * @param xsd a Source object representing the XSD file
     * @param xml a Node object representing the root Node of a XML document
     * @param schemaLanguage the XML Schema language to be used (preferable W3C
     * for best compatibility)
     * @param subNode a Node object representing a sub node of the XML document
     * @return a List object holding Strings containing the exception messages
     * @throws SAXException
     * @throws IOException
     * @throws TransformerException
     */
    private List<String> validate(Source xsd, Node xml, String schemaLanguage, Node subNode) throws SAXException, IOException, TransformerException {

        // clear the error message list
        errors.clear();

        // create a new XML Schema validator
        SchemaFactory factory = SchemaFactory.newInstance(schemaLanguage);
        Schema schema = factory.newSchema(xsd);
        Validator validator = schema.newValidator();

        // add a ValidationErrorHandler and validate
        validator.setErrorHandler(new ValidationErrorHandler(validator, subNode));
        validator.validate(new DOMSource(xml));
        return errors;
    }

    /**
     * Validate the whole XML Document and return formatted errors
     *
     * @return a String containing all validation errors found or an error
     * message in case validation went wrong
     */
    public String validateDocument() {
        try {
            List<String> validationList = validate(new DOMSource(xsdReader.getDoc()),
                    rootNode, schemaLanguage, null);

            if (validationList.size() > 0) {
                String msg = "";
                return validationList.stream().map((ex) -> ex + "\n").reduce(msg, String::concat);
            } else {
                return null;
            }
        } catch (SAXException | IOException | TransformerException ex) {
            Logger.getLogger(WebXMLAccesser.class.getName()).log(Level.SEVERE, null, ex);
            return ex.getLocalizedMessage();
        }
    }

    /**
     * Check if the content of a text Element or value of an Attribute is valid
     *
     * @return a String containing the error messages found, or null there are
     * no errors
     */
    public String isValidContent() {

        try {
            List<String> errorList;
            errorList = validate(new DOMSource(xsdReader.getDoc()), rootNode,
                    schemaLanguage, current);
            if (errorList.size() > 0) {
                String errStr = "";
                XSSimpleTypeDefinition td = null;
                if (current.getNodeType() == Node.ELEMENT_NODE) {
                    XSElementDeclaration xsdElement = (XSElementDeclaration) xsdReader.getXSDElement(current, false);
                    if (xsdElement.getTypeDefinition().getTypeCategory() == XSTypeDefinition.SIMPLE_TYPE) {
                        td = (XSSimpleTypeDefinition) xsdElement.getTypeDefinition();
                    }
                }

                if (current.getNodeType() == Node.ATTRIBUTE_NODE) {
                    td = xsdReader.getAttributeType(((AttrImpl) current).getOwnerElement(), ((AttrImpl) current).getName());
                }

                if (td != null) {
                    ArrayList<String> annotations = getAnnotations(td.getAnnotations());
                    if (annotations.size() > 0) {
                        errStr = annotations.stream().map((str) -> str + "\n").reduce(errStr, String::concat);
                    }
                }
                return errorList.stream().map((string) -> string + "\n").reduce(errStr, String::concat);
            } else {
                return null;
            }
        } catch (SAXException | TransformerException | IOException | ParserConfigurationException ex) {
            Logger.getLogger(WebXMLAccesser.class.getName()).log(Level.SEVERE, null, ex);
            return ex.getLocalizedMessage();
        }
    }

    /**
     * Get Elements that can be added to a parent Node
     *
     * @return an ArrayList holding all addable child Elements, or an empty
     * ArrayList if no addable nodes are found
     */
    public ArrayList<AddableElement> getAddableChildNodes() {
        ArrayList<AddableElement> addableNodes = new ArrayList<>();
        XSObject xsdElement = xsdReader.getXSDElement(current, false);
        if (xsdElement != null) {
            XSObjectList xsdChildElements = xsdReader.getXSDChildElements((XSElementDeclaration) xsdElement);
            if (xsdChildElements != null) {
                for (int i = 0; i < xsdChildElements.getLength(); i++) {
                    XSParticle particle = (XSParticle) xsdChildElements.item(i);
                    ArrayList<Element> childNodes = getChildElements(current, particle.getTerm().getName());

                    if (childNodes.size() < particle.getMaxOccurs() || particle.getMaxOccurs() == -1) {
                        addableNodes.add(new AddableElement(particle, xsdChildElements, i));
                    }
                }
            }
        }
        return addableNodes;
    }

    /**
     * Get Attributes that can be added to an owner Element
     *
     * @return an ArrayList holding all addable Attributes, or an empty
     * ArrayList if no addable attributes are found
     */
    public ArrayList<XSAttributeUse> getAddableAttributes() {

        ArrayList<XSAttributeUse> addableAttributes = new ArrayList<>();
        XSObjectList attributeUses = xsdReader.getAttributeUses(current);
        if (attributeUses != null) {
            for (int i = 0; i < attributeUses.getLength(); i++) {
                XSAttributeUse use = (XSAttributeUse) attributeUses.item(i);
                if (!((Element) current).hasAttribute(use.getAttrDeclaration().getName())) {
                    addableAttributes.add(use);
                }
            }
        }
        return addableAttributes;
    }

    /**
     * Add an Element to the XML
     *
     * @param addableNode a AddableNode object containing the Node and
     * additional information
     * @return an AddedElement object holding the ELement added and additional
     * information
     * @throws TransformerException
     */
    public AddedElement addElement(AddableElement addableNode) throws TransformerException {

        XSParticle particle = addableNode.getAddableNode();
        if (particle != null && particle.getTerm().getType() == XSConstants.ELEMENT_DECLARATION) {

            XSElementDeclaration childDecl = (XSElementDeclaration) particle.getTerm();
            Element cR = xsdReader.createRecursively(childDecl, doc);

            if ((addableNode.getXsdChildElements().getLength() - 1) == addableNode.getIndex()) {
                current.appendChild(cR);
                int position = getChildElements(current, null).size() - 1;
                return new AddedElement(cR, position);
            }

            ArrayList<Element> childNodes = getChildElements(current, null);
            XSObjectList xsdChildElements = addableNode.getXsdChildElements();

            for (int j = addableNode.getIndex() + 1; j < xsdChildElements.getLength(); j++) {
                XSParticle childParticle = (XSParticle) xsdChildElements.item(j);
                for (int i = 0; i < childNodes.size(); i++) {
                    if (childParticle.getTerm().getName().equals(childNodes.get(i).getTagName())) {
                        current.insertBefore(cR, childNodes.get(i));
                        return new AddedElement(cR, i);
                    }
                }
            }

            current.appendChild(cR);
            int position = childNodes.size();
            return new AddedElement(cR, position);
        }
        return null;
    }

    /**
     * Add an Attribute to an Element
     *
     * @param attrUse the XSAttributeUse of the Element declaration
     * @return a String representing the name of the attribute added
     */
    public String addAttribute(XSAttributeUse attrUse) {

        String checkSimpleType = xsdReader.checkSimpleType(attrUse.getConstraintType(), attrUse);
        ((Element) current).setAttributeNS(attrUse.getNamespace(),
                attrUse.getAttrDeclaration().getName(), checkSimpleType);
        return attrUse.getAttrDeclaration().getName();
    }

    /**
     * Check if an Element is removable
     *
     * @param element the Element to be removed
     * @return true if removable, otherwise false
     */
    public boolean isRemovableElement(Element element) {

        XSObject xsdElement = xsdReader.getXSDElement(element, true);
        if (xsdElement != null) {
            if (xsdElement.getType() == XSConstants.PARTICLE) {
                XSParticle particle = (XSParticle) xsdElement;
                ArrayList<Element> childNodesSameName = getChildElements(element.getParentNode(), element.getNodeName());
                return childNodesSameName.size() > particle.getMinOccurs();
            } else {
                return false;
            }
        } else {
            return true;
        }
    }

    /**
     * Check if an Attribute is removable
     *
     * @param ownerElement the Element holding the attribute
     * @param attrName the name of the Attribute
     * @return true if removable, otherwise false
     */
    public boolean isRemovableAttribute(Element ownerElement, String attrName) {

        XSAttributeUse attributeUse = xsdReader.getAttributeUse(ownerElement, attrName);
        if (attributeUse != null) {
            return !(attributeUse.getRequired());
        }
        return true;
    }

    /**
     * Check if an Attribute is of type ID
     *
     * @param element the Element holding the Attribute
     * @param attrName the name of the Attribute
     * @return true if ID, otherwise false
     */
    public boolean isID(Element element, String attrName) {
        XSSimpleTypeDefinition attributeType = xsdReader.getAttributeType(element, attrName);
        return attributeType != null && attributeType.getBuiltInKind() == XSConstants.ID_DT;
    }

    /**
     * Check if a simple type has an enumeration constraint and return the items
     *
     * @return a List holding the enumeration, an empty enumeration if no such
     * constraint exists or null if no simple type
     */
    public List<String> getEnum() {
        if (current.getNodeType() == Node.ELEMENT_NODE) {
            XSElementDeclaration xsdElement = (XSElementDeclaration) xsdReader.getXSDElement(current, false);
            if (xsdElement.getTypeDefinition().getTypeCategory() == XSTypeDefinition.SIMPLE_TYPE) {
                XSSimpleTypeDefinition typeDefinition = (XSSimpleTypeDefinition) xsdElement.getTypeDefinition();
                return typeDefinition.getLexicalEnumeration();
            }
        }

        if (current.getNodeType() == Node.ATTRIBUTE_NODE) {
            XSSimpleTypeDefinition typeDefinition
                    = xsdReader.getAttributeType(((AttrImpl) current).getOwnerElement(), ((AttrImpl) current).getName());
            if (typeDefinition != null) {
                return typeDefinition.getLexicalEnumeration();
            }
        }
        return null;
    }
}
