/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package webxmlaccesser;

import java.io.File;
import java.io.IOException;
import java.math.BigInteger;
import java.security.SecureRandom;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.apache.xerces.impl.xs.XSImplementationImpl;
import org.apache.xerces.xs.StringList;
import org.apache.xerces.xs.XSAttributeUse;
import org.apache.xerces.xs.XSComplexTypeDefinition;
import org.apache.xerces.xs.XSConstants;
import org.apache.xerces.xs.XSElementDeclaration;
import org.apache.xerces.xs.XSLoader;
import org.apache.xerces.xs.XSModel;
import org.apache.xerces.xs.XSModelGroup;
import org.apache.xerces.xs.XSNamedMap;
import org.apache.xerces.xs.XSObject;
import org.apache.xerces.xs.XSObjectList;
import org.apache.xerces.xs.XSParticle;
import org.apache.xerces.xs.XSSimpleTypeDefinition;
import org.apache.xerces.xs.XSTerm;
import org.apache.xerces.xs.XSTypeDefinition;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.bootstrap.DOMImplementationRegistry;
import org.xml.sax.SAXException;

/**
 * Class for handling XSD interaction
 *
 * @author koutsch
 */
public class XSDReader extends XReader {

    /**
     * Map containing the XSD root nodes (is in fact always the first node)
     */
    protected XSNamedMap rootNodeMap;

    private XSDReader() {
    }

    /**
     * Create a new XSSDReader with XML Schema file loaded
     *
     * @param file a file representing a XML Schema
     * @return a new XSD Reader with loaded XML Schema
     * @throws ClassNotFoundException
     * @throws InstantiationException
     * @throws IllegalAccessException
     * @throws SAXException
     * @throws IOException
     * @throws ParserConfigurationException
     */
    public static XSDReader instantiate(File file) throws ClassNotFoundException, InstantiationException, IllegalAccessException, SAXException, IOException, ParserConfigurationException {
        XSDReader xsdReader = new XSDReader();
        boolean loaded = xsdReader.getRootElementsFromFile(file);
        if (loaded) {
            return xsdReader;
        } else {
            return null;
        }
    }

    /**
     *
     * @param file a file representing a XML Schema
     * @return true if Schema successfully loaded
     * @throws ClassNotFoundException
     * @throws InstantiationException
     * @throws IllegalAccessException
     * @throws SAXException
     * @throws IOException
     * @throws ParserConfigurationException
     */
    private boolean getRootElementsFromFile(File file) throws ClassNotFoundException, InstantiationException,
            IllegalAccessException, SAXException, IOException, ParserConfigurationException {
        DOMImplementationRegistry registry = DOMImplementationRegistry.newInstance();
        XSImplementationImpl impl = (XSImplementationImpl) registry.getDOMImplementation("XS-Loader");
        XSLoader schemaLoader = impl.createXSLoader(null);
        XSModel model = schemaLoader.loadURI(file.getAbsolutePath());

        if (model != null) {
            rootNodeMap = model.getComponents(XSConstants.ELEMENT_DECLARATION);
            doc = loadXSDasDocument(file);
            return true;
        }
        return false;
    }

    private Document loadXSDasDocument(File file) throws SAXException, IOException, ParserConfigurationException {

        DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
        docFactory.setNamespaceAware(true);
        DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
        return docBuilder.parse(file);
    }

    /**
     * Create a new default XML Document according to the XML Schema loaded
     *
     * @param index the index of the root element declaration
     * @return a new Document representing the XML
     * @throws ParserConfigurationException
     */
    public Document createBasicXMLFromXSD(int index) throws ParserConfigurationException {
        XSElementDeclaration chosenRootElement = (XSElementDeclaration) rootNodeMap.item(index);
        Document xmlDoc = createDocument();
        Element cR = createRecursively(chosenRootElement, xmlDoc);
        xmlDoc.appendChild(cR);
        cR.setAttribute("xmlns", chosenRootElement.getNamespace());
        return xmlDoc;
    }

    /**
     * Get the Attribute declarations of a Node
     *
     * @param node the Node to be checked
     * @return a XSObjectList representing the attributes
     */
    public XSObjectList getAttributeUses(Node node) {
        XSElementDeclaration xsdElement = (XSElementDeclaration) getXSDElement(node, false);
        XSTypeDefinition typeDefinition = xsdElement.getTypeDefinition();
        if (typeDefinition.getTypeCategory() == XSTypeDefinition.COMPLEX_TYPE) {
            XSComplexTypeDefinition comp = (XSComplexTypeDefinition) typeDefinition;
            return comp.getAttributeUses();
        }
        return null;
    }

    /**
     * Get the XSAttributeUse of a XSD element declaration
     *
     * @param node the XML Element to check
     * @param attrName the name of the attribute to check
     * @return a XSAttributeUse representing the respective attribute
     * declaration
     */
    public XSAttributeUse getAttributeUse(Node node, String attrName) {

        XSObjectList attributeUses = getAttributeUses(node);
        if (attributeUses != null) {
            for (int i = 0; i < attributeUses.getLength(); i++) {
                XSAttributeUse use = (XSAttributeUse) attributeUses.item(i);
                if (use.getAttrDeclaration().getName().equals(attrName)) {
                    return use;
                }
            }
        }
        return null;
    }

    /**
     * Get the XSSimpleTypeDefinition of an attribute
     *
     * @param element the Element holding the attribute
     * @param attrName the name of the attribute
     * @return
     */
    public XSSimpleTypeDefinition getAttributeType(Element element, String attrName) {

        XSAttributeUse attributeUse = getAttributeUse(element, attrName);
        if (attributeUse != null) {
            return attributeUse.getAttrDeclaration().getTypeDefinition();
        }
        return null;
    }

    /**
     * Get all parent nodes of a Node
     *
     * @param node the Node to be checked
     * @return an ArrayList holding all parent nodes
     */
    private ArrayList<Node> getLineage(Node node) {
        ArrayList<Node> lineage = new ArrayList<>();
        while (node.getParentNode() != null) {
            lineage.add(node);
            node = node.getParentNode();
        }
        return lineage;
    }

    /**
     * Obtain the root node declaration of a Node
     *
     * @param rootNode the node to be checked
     * @return the corresponding XSElementDeclaration
     */
    private XSElementDeclaration getRootNode(Node rootNode) {
        for (int i = 0; i < rootNodeMap.getLength(); i++) {
            if (rootNodeMap.item(i).getName().equals(rootNode.getNodeName())) {
                return (XSElementDeclaration) rootNodeMap.item(i);
            }
        }
        return null;
    }

    /**
     * Get the XML Schema declaration of a Node
     *
     * @param node the Node to check
     * @param getParticle if true the XSParticle is given, if false the
     * XSElementDeclaration
     * @return
     */
    public XSObject getXSDElement(Node node, boolean getParticle) {

        ArrayList<Node> lineage = getLineage(node);
        XSElementDeclaration elementDecl = getRootNode(lineage.get(lineage.size() - 1));
        if (elementDecl != null) {

            if (lineage.size() == 1) {
                return elementDecl;
            }

            for (int j = lineage.size() - 2; j >= 0; j--) {
                XSObjectList xsdChildElements = getXSDChildElements(elementDecl);
                if (xsdChildElements != null) {
                    for (int k = 0; k < xsdChildElements.getLength(); k++) {
                        XSParticle particle = (XSParticle) xsdChildElements.item(k);
                        XSTerm childTerm = particle.getTerm();
                        if (childTerm.getName().equals(lineage.get(j).getNodeName())) {
                            if (j == 0) {
                                if (getParticle) {
                                    return particle;
                                } else if (particle.getTerm().getType() == XSConstants.ELEMENT_DECLARATION) {
                                    return (XSElementDeclaration) particle.getTerm();
                                }
                            } else {
                                elementDecl = (XSElementDeclaration) childTerm;
                                break;
                            }
                        }
                    }
                }
            }
        }
        return null;
    }

    /**
     * Check a XSD simple type and generate default value
     *
     * @param constraintType the constraint type of an element text or attribute
     * value
     * @param declaration the XSSimpleTypeDefinition
     * @return a String representing the default value
     */
    public String checkSimpleType(short constraintType, XSObject declaration) {

        // check if there is default or fixed
        switch (constraintType) {
            case XSConstants.VC_DEFAULT:
            case XSConstants.VC_FIXED:
                switch (declaration.getType()) {
                    case XSConstants.ELEMENT_DECLARATION:
                        XSElementDeclaration XSElement = (XSElementDeclaration) declaration;
                        return XSElement.getValueConstraintValue().getActualValue().toString();
                    case XSConstants.ATTRIBUTE_USE:
                        XSAttributeUse XSAttributeUse = (XSAttributeUse) declaration;
                        return XSAttributeUse.getValueConstraintValue().getActualValue().toString();
                    default:
                        return null;
                }
            // if not put logic
            case XSConstants.VC_NONE:
                XSSimpleTypeDefinition xstd = null;
                switch (declaration.getType()) {
                    case XSConstants.ELEMENT_DECLARATION:
                        XSElementDeclaration XSElement = (XSElementDeclaration) declaration;
                        switch (XSElement.getTypeDefinition().getTypeCategory()) {
                            case XSTypeDefinition.COMPLEX_TYPE:
                                xstd = ((XSComplexTypeDefinition) XSElement.getTypeDefinition()).getSimpleType();
                                break;
                            case XSTypeDefinition.SIMPLE_TYPE:
                                xstd = (XSSimpleTypeDefinition) XSElement.getTypeDefinition();
                                break;
                        }
                        break;
                    case XSConstants.ATTRIBUTE_USE:
                        XSAttributeUse XSAttributeUse = (XSAttributeUse) declaration;
                        xstd = XSAttributeUse.getAttrDeclaration().getTypeDefinition();
                        break;
                    default:
                        return null;
                }
                if (xstd != null) {
                    // check if enumeration and if yes assign first value
                    StringList lexicalEnumeration = xstd.getLexicalEnumeration();
                    if (lexicalEnumeration.size() > 0) {
                        return lexicalEnumeration.item(0);
                    } else {
                        // if not switch through the defaults
                        switch (xstd.getBuiltInKind()) {
                            case XSConstants.STRING_DT:
                            default:
                                return "TO BE FILLED";
                            case XSConstants.INTEGER_DT:
                            case XSConstants.FLOAT_DT:
                            case XSConstants.DOUBLE_DT:
                            case XSConstants.LONG_DT:
                                return "123";
                            case XSConstants.ANYURI_DT:
                                return "http://www.example.com";
                            case XSConstants.DATE_DT:
                                DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                                Date date = new Date();
                                return dateFormat.format(date);
                            case XSConstants.TIME_DT:
                                DateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
                                Date time = new Date();
                                return timeFormat.format(time);
                            case XSConstants.ID_DT:
                                SecureRandom random = new SecureRandom();
                                return new BigInteger(130, random).toString(32).substring(0, 10);
                        }
                    }
                }
        }
        return null;
    }

    /**
     * Create a new default XML document or node according to a XML Schema
     *
     * @param term the XSTerm containing the respective XML Schema
     * @param doc the Document the XML node(s) create are to be added to
     * @return a new Element representing the XML
     */
    public Element createRecursively(XSTerm term, Document doc) {

        if (term.getType() == XSConstants.ELEMENT_DECLARATION) {
            XSElementDeclaration XSDElement = (XSElementDeclaration) term;
            Element XMLElement = doc.createElementNS(XSDElement.getNamespace(),
                    XSDElement.getName());
            XSTypeDefinition typeDefinition = XSDElement.getTypeDefinition();

            switch (typeDefinition.getTypeCategory()) {
                case XSTypeDefinition.COMPLEX_TYPE:
                    XSComplexTypeDefinition comp = (XSComplexTypeDefinition) typeDefinition;

                    // add attributes
                    XSObjectList attributeUses = comp.getAttributeUses();
                    for (int i = 0; i < attributeUses.getLength(); i++) {
                        XSAttributeUse use = (XSAttributeUse) attributeUses.item(i);
                        XMLElement.setAttributeNS(use.getNamespace(),
                                use.getAttrDeclaration().getName(),
                                checkSimpleType(use.getConstraintType(), use));
                    }

                    XSParticle particle = comp.getParticle();
                    if (particle != null) {
                        switch (particle.getTerm().getType()) {

                            case XSConstants.MODEL_GROUP:
                                XSModelGroup modelGroup = (XSModelGroup) particle.getTerm();
                                XSObjectList particles = modelGroup.getParticles();
                                if (particles != null) {

                                    for (int i = 0; i < particles.getLength(); i++) {
                                        XSParticle item = (XSParticle) particles.item(i);
                                        XSTerm childTerm = item.getTerm();
                                        Element cR = createRecursively(childTerm, doc);
                                        XMLElement.appendChild(cR);
                                    }
                                }
                                break;
                        }
                    } else {
                        switch (comp.getContentType()) {
                            case XSConstants.DERIVATION_EXTENSION:
                            case XSConstants.DERIVATION_RESTRICTION:
                                XMLElement.setTextContent(checkSimpleType(XSDElement.getConstraintType(), XSDElement));
                                break;
                        }
                    }
                    break;
                case XSTypeDefinition.SIMPLE_TYPE:
                    XMLElement.setTextContent(checkSimpleType(XSDElement.getConstraintType(), XSDElement));
                default:
                    break;
            }
            return XMLElement;
        }
        return null;
    }

    /**
     * Get the XSParticleTerm from a XSElementDeclaration
     *
     * @param xsElementDecl the XSElementDeclaration
     * @return the extracted XSTerm from the corresponding XSParticle
     */
    private XSTerm getParticleTerm(XSElementDeclaration xsElementDecl) {

        if (xsElementDecl != null) {
            XSTypeDefinition typeDefinition = xsElementDecl.getTypeDefinition();
            if (typeDefinition.getTypeCategory() == XSTypeDefinition.COMPLEX_TYPE) {
                XSComplexTypeDefinition comp = (XSComplexTypeDefinition) typeDefinition;
                XSParticle particle = comp.getParticle();
                if (particle != null) {
                    XSTerm term = particle.getTerm();
                    return term;
                }
            }
        }
        return null;
    }

    /**
     * Get the child element declarations of an XSElementDeclaration
     *
     * @param elementDecl the XSElementDeclaration
     * @return an XSObjectList holding the child element declarations
     */
    public XSObjectList getXSDChildElements(XSElementDeclaration elementDecl) {
        XSTerm term = getParticleTerm(elementDecl);
        if (term != null && term.getType() == XSConstants.MODEL_GROUP) {
            XSModelGroup modelGroup = (XSModelGroup) term;
            return modelGroup.getParticles();
        }
        return null;
    }
}
