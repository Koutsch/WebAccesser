/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataObjects;

import org.apache.xerces.xs.XSObjectList;
import org.apache.xerces.xs.XSParticle;

/**
 *
 * @author koutsch
 */
public class AddableElement {

    XSParticle addableNode;
    XSObjectList xsdChildElements;
    int index;

    public AddableElement(XSParticle addableNode, XSObjectList xsdChildElements, int index) {
        this.addableNode = addableNode;
        this.xsdChildElements = xsdChildElements;
        this.index = index;
    }


    public XSParticle getAddableNode() {
        return addableNode;
    }

    public XSObjectList getXsdChildElements() {
        return xsdChildElements;
    }

    public int getIndex() {
        return index;
    }
}
