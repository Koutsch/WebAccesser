/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataObjects;

import java.io.File;

/**
 *
 * @author koutsch
 */
public class ServerDownload {
    File file;
    String serverPath;
    ServerLoginData loginData;

    public ServerDownload(File file, String serverPath, ServerLoginData loginData) {
        this.file = file;
        this.serverPath = serverPath;
        this.loginData = loginData;
    }

    public File getFile() {
        return file;
    }

    public String getServerPath() {
        return serverPath;
    }

    public ServerLoginData getLoginData() {
        return loginData;
    }
}
