/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataObjects;

import java.util.ArrayList;
import org.apache.commons.net.ftp.FTPFile;

/**
 *
 * @author koutsch
 */
public class FTPDirectory {
    String name;
    ArrayList<FTPFile> files;

    public FTPDirectory(String name, ArrayList<FTPFile> files) {
        this.name = name;
        this.files = files;
    }

    public String getName() {
        return name;
    }

    public ArrayList<FTPFile> getFiles() {
        return files;
    }
}
