/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataObjects;

import org.w3c.dom.Element;

/**
 *
 * @author koutsch
 */
public class AddedElement {
    
    Element element;
    int position;

    public AddedElement(Element element, int position) {
        this.element = element;
        this.position = position;
    }

    public Element getElement() {
        return element;
    }

    public int getPosition() {
        return position;
    }
}
