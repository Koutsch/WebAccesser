/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import java.util.ArrayList;
import javax.swing.DefaultComboBoxModel;
import dataObjects.AddableElement;
import org.apache.xerces.xs.XSAttributeUse;

/**
 *
 * @author koutsch
 */
public class AddDialog extends javax.swing.JDialog {

    private final ArrayList<AddableElement> addableNodes;
    private final ArrayList<XSAttributeUse> addableAttributes;
    private Object chosenNode;
    DefaultComboBoxModel<String> model;

    /**
     * Creates new form AddDialog
     *
     * @param parent
     * @param addableNodes
     * @param addableAttributes
     * @param modal
     */
    public AddDialog(java.awt.Frame parent, ArrayList<AddableElement> addableNodes,
            ArrayList<XSAttributeUse> addableAttributes, boolean modal) {
        super(parent, modal);

        initComponents();

        setLocationRelativeTo(null);

        this.model = new DefaultComboBoxModel<>();
        this.addableNodes = addableNodes;
        this.addableAttributes = addableAttributes;

        if (addableNodes != null) {
            addableNodes.stream().forEach((addableNode) -> {
                model.addElement(addableNode.getAddableNode().getTerm().getName());
            });
        }
        if (addableAttributes != null) {
            addableAttributes.stream().forEach((addableAttribute) -> {
                model.addElement(addableAttribute.getAttrDeclaration().getName());
            });
        }

        comboBox.setModel(model);
    }

    public Object showDialog() {
        setVisible(true);
        return chosenNode;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        chooseButton = new javax.swing.JButton();
        cancelButton = new javax.swing.JButton();
        comboBox = new javax.swing.JComboBox<>();
        jSeparator1 = new javax.swing.JSeparator();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Dazuatuan");
        setResizable(false);

        chooseButton.setText("Des nimm i!");
        chooseButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chooseButtonActionPerformed(evt);
            }
        });

        cancelButton.setText("A, liawa obbrechn...");
        cancelButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelButtonActionPerformed(evt);
            }
        });

        jLabel2.setText("1) Schau, dasst die richtige Kategorie ausgwöhlt host und...");

        jLabel3.setText("2) Suach die richtige Unterkategorie aus wost dazuafügn wüllst:");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(comboBox, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, 498, Short.MAX_VALUE)
                    .addComponent(jSeparator1)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(chooseButton)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cancelButton)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(comboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(chooseButton)
                    .addComponent(cancelButton))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void cancelButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cancelButtonActionPerformed
        // TODO add your handling code here:
        chosenNode = null;
        this.setVisible(false);
        this.dispose();
    }//GEN-LAST:event_cancelButtonActionPerformed

    private void chooseButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chooseButtonActionPerformed
        // TODO add your handling code here:
        int selectedIndex = comboBox.getSelectedIndex();
        int addNodeSize = 0;
        if (addableNodes != null) {
            addNodeSize = addableNodes.size();
        }
        if (selectedIndex < addNodeSize) {
            chosenNode = (AddableElement) addableNodes.get(selectedIndex);
        } else {
            chosenNode = (XSAttributeUse) addableAttributes.get(selectedIndex - addNodeSize);
        }
        this.setVisible(false);
        this.dispose();
    }//GEN-LAST:event_chooseButtonActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton cancelButton;
    private javax.swing.JButton chooseButton;
    private javax.swing.JComboBox<String> comboBox;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JSeparator jSeparator1;
    // End of variables declaration//GEN-END:variables
}
