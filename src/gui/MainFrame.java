/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JTree;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import org.apache.commons.io.FileUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;
import dataObjects.AddableElement;
import dataObjects.AddedElement;
import dataObjects.ServerDownload;
import dataObjects.ServerLoginData;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.WindowListener;
import java.net.URL;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Enumeration;
import java.util.List;
import javax.swing.AbstractAction;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import static javax.swing.JFrame.EXIT_ON_CLOSE;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.KeyStroke;
import javax.swing.tree.DefaultTreeCellRenderer;
import org.apache.xerces.dom.AttrImpl;
import org.apache.xerces.xs.XSAttributeUse;
import org.w3c.dom.NamedNodeMap;
import webxmlaccesser.WebXMLAccesser;
import webxmlaccesser.XMLReader;
import webxmlaccesser.XSDReader;

/**
 *
 * @author koutsch
 */
public class MainFrame extends javax.swing.JFrame {

    private DefaultTreeModel model;
    private JList list;
    private boolean isEnum = false;
    private XMLTreeNode rootNode;
    private XMLReader xr;
    private JTree tree;
    private ArrayList<AddableElement> addableChildNodes;
    private ArrayList<XSAttributeUse> addableAttributes;
    private final PreviousActions undoList = new PreviousActions(20);
    private ServerLoginData loginData;
    private File chosenFile;
    private String serverPath;
    public static final String TEMP_PATH
            = WebXMLAccesser.getHomeDirectory()
            + File.separator + "temp_files";
    private TreeAndSearchPanel tasp;
    private final Font font = new Font("Verdana", Font.PLAIN, 14);
    private XMLTreeNode oldSelection;

    /* GUI texts */
    private final String CLOSE_TEXT = "Aufhean? Host eh ois ogspeichert, gö?";
    private final String CLOSE_TITLE = "Aufhean";
    private final String WRONG_CONTENT_TITLE = "Inhoit passt net";
    private final String ERROR_TITLE = "Föülla";
    private final String NO_CONTENT_TEXT = "Heast, einischreibn muaßt scho wos!";
    private final String NO_CONTENT_TITLE = "Text vagessn";
    private final String ADD_NODE_ERROR_TEXT = "Hob's net dazuatuan kinnan! Föülla:\n";
    private final String ADD_NODE_ERROR_TITLE = "Dazuatuan geht net";
    private final String WANT_DELETE_TEXT = "Leschn? Bist da sicha, dasst des wüllst?";
    private final String WANT_DELETE_TITLE = "Leschn";
    private final String CONTENT_OF_TEXT = "Textinhoit va ";
    private final String NO_CONTENT_ELEMENT_TEXT = "Hot kan Textinhoit.";
    private final String ERROR_TEXT = "Föülla:\n";
    private final String CANNOT_CHOOSE_TEXT = "Kau nix auswöhln";
    private final String SERVER_ONLY_TEXT = "Douz geht nur wennst mitn Server vabundn bist!";
    private final String NO_SERVER_CONNECTION_TITLE = "Ka Vabindung zan Server";
    private final String INVALID_TEXT = "XML-Validierung hot ergebn, dass wos net passt:\n";
    private final String INVALID_TITLE = "Föülla ban Validian";
    private final String VALID_TEXT = "XML is validiat, passt!";
    private final String VALID_TITLE = "Validiat";
    private final String WANT_NEW_TEXT = "Du host do nou wos ouffn. Wennst a neix XML mocht, geht des valuan! Wüllst ejcht a neix XML mochn?";
    private final String WANT_NEW_TITLE = "Neiche Datei aufmochn";
    private final String ERROR_LOAD_XML_TEXT = "Föülla ban Lodn, as XML is net valide. Im CMD öffnan und schau wos'a sogt!";
    private final String ERROR_LOAD_XSD_TEXT = "Föülla ban Lodn, as XSD is net valide. Im CMD öffnan und schau wos'a sogt!";
    private final String ERROR_NEW_TITLE = "Kau ka neix mochn";
    private final String FROM_SERVER_OPTION = "Vom Server";
    private final String FROM_LOCAL_OPTION = "Va ana Datää aum Computer";
    private final String CANCEL_OPTION = "Obbrechn";
    private final String CHOOSE_SOURCE_TEXT = "Wöhl aus va woust die XML-Datää öffnan wüllst:";
    private final String CHOOSE_SOURCE_TITLE = "XML Datää öffnan";
    private final String OPEN_XSD_FILE_TITLE = "XSD öffnan";
    private final String XSD_EXTENSION_TEXT = "XSD Datään";
    private final String XSD_EXTENSION_TYPE = "xsd";
    private final String OPEN_XML_FILE_TITLE = "XML öffnan";
    private final String XML_EXTENSION_TEXT = "XML Datään";
    private final String XML_EXTENSION_TYPE = "xml";
    private final String ERROR_LOAD_XML_OR_XSD_TEXT = "Föülla ban Lodn, as XSD oda XML is (oda olle zwa san) net valide. Schau im Log!";
    private final String OPEN_XML_ERROR_TITLE = "Kau i net öffnan";
    private final String BOLD_TEXT = "<b>DO EINISCHREIBN</b>";
    private final String ITALICS_TEXT = "<i>DO EINISCHREIBN</i>";
    private final String NEW_LINE_TEXT = "<br />";
    private final String UPLOAD_ERROR_TEXT = "S' hot a problem gebn ban Aufilodn! Föülla:\n";
    private final String SAVE_ERROR_TEXT = "XML hot gspeichert werdn kinnan! Föülla:\n";
    private final String NO_UPLOAD_TEXT = "XML hot nochn speichan net aufiglodn werdn kinnan!";
    private final String DOWNLOAD_ERROR_TEXT = "S' hot a Problem gebn ban Owilodn! Föülla:\n";
    private final String SAVE_CHANGE_TEXT = "Du host'n Text gändat owa net iwanoumman. Wüst de Ändarung ospeichern?";
    private final String SAVE_CHANGE_TITLE = "Text gändat";

    /**
     * Creates new form mainFrame
     *
     */
    public MainFrame() {

        // Set icon
        URL iconURL = getClass().getResource("/webxmlaccesser/Icon.png");
        ImageIcon icon = new ImageIcon(iconURL);
        this.setIconImage(icon.getImage());

        // initiate pre-given components
        initComponents();
        // appear in the middle upon start
        setLocationRelativeTo(null);

        // register button listeners
        editButton.addActionListener(editListener);
        deleteButton.addActionListener(deleteListener);
        addButton.addActionListener(addListener);

        // set split divider location to one third to the left
        splitPane.setDividerLocation(getWidth() / 3);

        // add "Close?" dialog
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {

                MainFrame frame = (MainFrame) e.getSource();

                int result = JOptionPane.showConfirmDialog(
                        frame,
                        CLOSE_TEXT,
                        CLOSE_TITLE,
                        JOptionPane.YES_NO_OPTION);

                if (result == JOptionPane.YES_OPTION) {
                    try {
                        clearTemp();
                    } catch (IOException ex) {
                        Logger.getLogger(WebXMLAccesser.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    frame.setDefaultCloseOperation(EXIT_ON_CLOSE);
                }
            }
        });

        // set keyboard shortcuts for buttons
        setButtonShortcuts();
        // set the font of the main text area
        textArea.setFont(font);
    }

    @Override
    public final void setLocationRelativeTo(Component c) {
        super.setLocationRelativeTo(c);
    }

    @Override
    public final int getWidth() {
        return super.getWidth();
    }

    @Override
    public final synchronized void addWindowListener(WindowListener l) {
        super.addWindowListener(l);
    }

    /**
     * Adds keyboard shortcuts for the buttons in the Main Frame
     */
    private void setButtonShortcuts() {

        KeyStroke editStroke = KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_E,
                java.awt.event.InputEvent.CTRL_MASK);
        editButton.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW)
                .put(editStroke, "edit");
        editButton.getActionMap().put("edit",
                new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                edit(null);
            }
        });

        KeyStroke addStroke = KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_D,
                java.awt.event.InputEvent.CTRL_MASK);
        addButton.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW)
                .put(addStroke, "add");
        addButton.getActionMap().put("add",
                new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                add();
            }
        });

        KeyStroke deleteStroke = KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_L,
                java.awt.event.InputEvent.CTRL_MASK);
        deleteButton.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW)
                .put(deleteStroke, "delete");
        deleteButton.getActionMap().put("delete",
                new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                delete();
            }
        });
    }

    /**
     * Loads a tree from a given XMLReader
     *
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws IOException
     * @throws ClassNotFoundException
     * @throws InstantiationException
     * @throws IllegalAccessException
     */
    private void loadTree() throws ParserConfigurationException,
            SAXException, IOException, ClassNotFoundException,
            InstantiationException, IllegalAccessException {

        // assign the root element from the XML
        rootNode = addData(xr.getRootNode());
        // assign the root treeNode to the tree model
        model = new DefaultTreeModel(rootNode);
        // instantiate tree with tree model
        tree = new JTree(model);
        // set the custom cell renderer
        tree.setCellRenderer(new CustomTreeCellRenderer());
        // forbid editing of tree
        tree.setEditable(false);
        // only one treeNode at a time can be selected
        tree.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
        // add custom selection listener
        tree.addTreeSelectionListener(new CustomTreeSelectionListener());

        // instantiate and show new tree and search panel
        tasp = new TreeAndSearchPanel();
        tasp.getTreeScrollPane().setViewportView(tree);

        // register keyboard shortcuts for tree and search panel
        KeyStroke searchStroke = KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_ENTER, 0);
        tasp.getSearchTextField().getInputMap(JComponent.WHEN_FOCUSED)
                .put(searchStroke, "search");
        tasp.getSearchTextField().getActionMap().put("search", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                search();
            }
        });

        tasp.getClearSearchButton().addActionListener((ActionEvent e) -> {
            Enumeration preorderEnumeration = rootNode.preorderEnumeration();
            while (preorderEnumeration.hasMoreElements()) {
                XMLTreeNode node = (XMLTreeNode) preorderEnumeration.nextElement();
                node.setFound(false);
                model.reload();
            }
        });

        tasp.getSearchModeButton().addActionListener((ActionEvent e) -> {
            JButton source = (JButton) e.getSource();
            String text = source.getText();
            switch (text) {
                case "O":
                    source.setText("E");
                    break;
                case "E":
                    source.setText("T");
                    break;
                case "T":
                default:
                    source.setText("O");
                    break;
            }
        });

        // add the tree and search panel to the left
        splitPane.setTopComponent(tasp);
        // set the split pane divider one more to a third to the left
        splitPane.setDividerLocation(this.getWidth() / 3);
        // set the font of the search field
        tasp.getSearchTextField().setFont(font);
    }

    /**
     * A custom tree cell renderer for extracting the treeNode name and
     * assigning different colors to the different treeNode types
     */
    public class CustomTreeCellRenderer extends DefaultTreeCellRenderer {

        // no border hwen selected
        @Override
        public Color getBorderSelectionColor() {
            return null;
        }

        // the actual logic
        @Override
        public Component getTreeCellRendererComponent(JTree tree, Object value,
                boolean sel, boolean exp, boolean leaf, int row, boolean hasFocus) {

            // get the XMLTreeNode and the node contained
            XMLTreeNode treeNode = (XMLTreeNode) value;
            JComponent treeCellRendererComponent;
            Node docNode = (Node) treeNode.getUserObject();
            // get the node name
            String nodeName = docNode.getNodeName();
            // check if there is an ID node and if yes add it to the element label
            if (!(treeNode instanceof AttributeTreeNode) && docNode.hasAttributes()) {
                NamedNodeMap attributes = docNode.getAttributes();
                for (int i = 0; i < attributes.getLength(); i++) {
                    if (xr.isID(((Element) docNode), attributes.item(i).getNodeName())) {
                        nodeName += " " + attributes.item(i).getNodeName()
                                + ": " + attributes.item(i).getNodeValue();
                    }
                }
            }

            // get the treeCellRendererComponent and add the node name
            treeCellRendererComponent
                    = (JComponent) super.getTreeCellRendererComponent(tree, nodeName, sel, exp, leaf, row, hasFocus);

            // check if it was found during a search and add colors
            boolean found = treeNode.getFound();
            if (found) {
                treeCellRendererComponent.setForeground(Color.red);
            } else if (treeNode instanceof AttributeTreeNode) {
                treeCellRendererComponent.setForeground(Color.blue);
            } else if (treeNode instanceof TextElementTreeNode) {
                treeCellRendererComponent.setForeground(new Color(1, 115, 18));
            } else {
                treeCellRendererComponent.setForeground(Color.black);
            }
            // finish returning the formatted component
            return treeCellRendererComponent;
        }
    }

    /**
     * Add data to a node recursively
     *
     * @param XMLNode the Node to add subnodes or attributes to
     * @return a XMLTreeNode holding the data added
     */
    private XMLTreeNode addData(Node XMLNode) {

        // get all child elements of a node
        ArrayList<Element> childNodes = xr.getChildElements(XMLNode, null);
        // declare tree node and fill if there are child nodes and do so recursively
        XMLTreeNode treeNode;
        if (childNodes.size() > 0) {
            treeNode = new ElementTreeNode(XMLNode);
            childNodes.stream().map((childNode) -> addData(childNode)).forEach((addDataNew) -> {
                treeNode.add(addDataNew);
            });
            // otherwise make it  text node
        } else if (XMLNode.hasChildNodes()) {
            treeNode = new TextElementTreeNode(XMLNode);
            // or an empty element node if no children at all
        } else {
            treeNode = new ElementTreeNode(XMLNode);
        }
        // add the attributes
        NamedNodeMap attributes = XMLNode.getAttributes();
        for (int i = 0; i < attributes.getLength(); i++) {
            treeNode.add(new AttributeTreeNode(attributes.item(i)));
        }
        return treeNode;
    }

    /**
     * Set the selection path to a tree node
     *
     * @param treeNode the XMLTreeNode to set the selection path to
     */
    private void setSelectedTreeNode(XMLTreeNode treeNode) {
        TreePath treePath = new TreePath(treeNode.getPath());
        tree.setSelectionPath(treePath);
    }

    /**
     * Edit the text content of an Element or value of an Attribute
     */
    private void edit(XMLTreeNode treeNode) {

        // get the text from the user
        String text;
        if (isEnum) {
            // if there is an enumaration constraint get it from the list
            text = list.getSelectedValue().toString();
        } else {
            // get text from main text area
            text = textArea.getText();
            // check that not empty
            if ("".equals(text)) {
                JOptionPane.showMessageDialog(rootPane,
                        NO_CONTENT_TEXT, NO_CONTENT_TITLE, JOptionPane.WARNING_MESSAGE);
                return;
            }
        }

        // save old node content in case not validationErrors
        String oldContent = xr.getCurrent().getTextContent();
        // change node content to new text
        xr.getCurrent().setTextContent(text);
        // declare possible error messages
        String errorMessages;
        errorMessages = xr.isValidContent();
        if (errorMessages != null) {
            xr.getCurrent().setTextContent(oldContent);
            ListDialog ld = new ListDialog(this, true);
            ld.setTitle(WRONG_CONTENT_TITLE);
            ld.getTextArea().setText(errorMessages);
            ld.setVisible(true);

            // Log that failed
            Logger.getLogger(WebXMLAccesser.class.getName()).log(Level.INFO, errorMessages, "Invalid content");
            // put back old text
            xr.getCurrent().setTextContent(oldContent);
            // if no errors proceed
        } else {
            // get the selected component or use the tree node given as parameter
            treeNode = (treeNode != null) ? treeNode : (XMLTreeNode) tree.getSelectionPath().getLastPathComponent();
            // reload model so ID changes are displayed in the tree GUI
            if (treeNode instanceof AttributeTreeNode) {
                XMLTreeNode parentTreeNode = (XMLTreeNode) treeNode.getParent();
                Node parentNode = (Node) parentTreeNode.getUserObject();
                AttrImpl attr = (AttrImpl) treeNode.getUserObject();
                if (xr.isID(((Element) parentNode), attr.getName())) {
                    model.reload(parentTreeNode);
                }
            }
            // register undo
            undoList.add(undoList.new EditedContent(oldContent, treeNode));
            // Log that editted
            Logger.getLogger(WebXMLAccesser.class.getName()).log(Level.INFO,
                    "Content changed to: " + text, "Node editted");
        }
    }

    /**
     * Add a new element or attribute
     */
    private void add() {
        // open add dialog to let user choose
        AddDialog addDialog = new AddDialog(null, addableChildNodes, addableAttributes, true);
        Object showDialog = addDialog.showDialog();

        // check if user chose
        if (showDialog != null) {
            // get selected tree node
            XMLTreeNode lastPathComponent = (XMLTreeNode) tree.getSelectionPath().getLastPathComponent();
            // check if user chose Element
            if (showDialog instanceof AddableElement) {
                try {
                    // add the element to the XML and retrieve add information
                    AddedElement addElement = xr.addElement(((AddableElement) showDialog));
                    // check if element was successfully added
                    if (addElement != null) {
                        // add the data to the tree node and receive the new tree node
                        XMLTreeNode newTreeNode = addData(addElement.getElement());
                        // add the new tree node to the parent node
                        lastPathComponent.insert(newTreeNode, addElement.getPosition());
                        // reload the parent node
                        model.reload(lastPathComponent);
                        // set the selection to the newly created tree node
                        setSelectedTreeNode(newTreeNode);
                        // add undo information
                        undoList.add(undoList.new AddedElement(newTreeNode));
                        // log that node added
                        Logger.getLogger(WebXMLAccesser.class.getName()).log(Level.INFO,
                                "New node " + addElement.getElement().getTagName()
                                + " added at position " + addElement.getPosition(), "Node added");
                    }
                    // report error went something went wrong
                } catch (TransformerException ex) {
                    JOptionPane.showMessageDialog(rootPane,
                            ADD_NODE_ERROR_TEXT + ex,
                            ADD_NODE_ERROR_TITLE, JOptionPane.WARNING_MESSAGE);
                    Logger.getLogger(WebXMLAccesser.class
                            .getName()).log(Level.SEVERE, null, ex);
                }
            }
            // check if user chose Element
            if (showDialog instanceof XSAttributeUse) {
                // add attribute to XML
                String addAttributeName = xr.addAttribute(((XSAttributeUse) showDialog));
                // get attributes of element where attribute has been added
                NamedNodeMap attributes = xr.getCurrent().getAttributes();

                // get the number of children of tree node
                int childTreeNodeCount = lastPathComponent.getChildCount();
                // get number of attributes
                int attrNodeLength = attributes.getLength();

                // loop through attributes
                for (int i = 0; i < attrNodeLength; i++) {
                    // if the name of the added attribute matches an attribute
                    if (attributes.item(i).getLocalName().equals(addAttributeName)) {
                        // create a new tree node holding the attribute
                        AttributeTreeNode treeNode = new AttributeTreeNode(attributes.item(i));
                        // calculate the position in the attribute AFTER the child elements
                        int diff = childTreeNodeCount - attrNodeLength;
                        // insert it one AFTER the position in the XML
                        lastPathComponent.insert(treeNode, (i + diff + 1));
                        // reload the parent node
                        model.reload(lastPathComponent);
                        // set the selection to the newly created tree node
                        setSelectedTreeNode(treeNode);
                        // add undo information
                        undoList.add(undoList.new AddedAttribute(treeNode));
                        // log that node added
                        Logger.getLogger(WebXMLAccesser.class.getName()).log(Level.INFO,
                                "New element " + addAttributeName
                                + " added", "Node added");
                        // break so the loop does not keep on looping unnecessarily
                        break;
                    }
                }
            }
        }
    }

    /**
     * Delete a node or attribute
     */
    private void delete() {

        // prompt user if really wants to delete
        int showConfirmDialog = JOptionPane.showConfirmDialog(this, WANT_DELETE_TEXT,
                WANT_DELETE_TITLE, JOptionPane.WARNING_MESSAGE);

        // check if user confirms
        if (showConfirmDialog == JOptionPane.OK_OPTION) {

            String nodeName = null;

            // get treeNode to delete, its parent node and the position of the deleted node
            TreePath selectionPath = tree.getSelectionPath();
            XMLTreeNode treeNode = (XMLTreeNode) selectionPath.getLastPathComponent();
            XMLTreeNode parentTreeNode = (XMLTreeNode) treeNode.getParent();
            int index = parentTreeNode.getIndex(treeNode);

            // prepare information for undo of Element removal
            // check if Text or Element tree node
            if (treeNode instanceof ElementTreeNode || treeNode instanceof TextElementTreeNode) {
                // get parent XML node
                Node parentNode = xr.getCurrent().getParentNode();
                // get XML sibling node
                Node nextSibling = xr.getCurrent().getNextSibling();
                // remove node from parent and retrieve removed node
                Node removeChild = parentNode.removeChild(xr.getCurrent());
                // add undo information
                undoList.add(undoList.new DeletedElement(parentNode,
                        removeChild, nextSibling, parentTreeNode, treeNode, index));
                // save name for log
                nodeName = xr.getCurrent().getNodeName();
            }

            // prepare information for undo of Attribute removal
            // check if Attribute tree node
            if (treeNode instanceof AttributeTreeNode) {
                AttrImpl attrNode = (AttrImpl) treeNode.getUserObject();
                // get the owner element of the attribute
                Element ownerElement = attrNode.getOwnerElement();
                // remove the attribute
                ownerElement.removeAttribute(attrNode.getNodeName());
                // add undo information
                undoList.add(undoList.new DeletedAttribute(ownerElement, attrNode,
                        index, parentTreeNode, treeNode));
                // save name for log
                nodeName = attrNode.getNodeName();
            }
            // remove tree node
            model.removeNodeFromParent(treeNode);
            // log that node removed
            Logger.getLogger(WebXMLAccesser.class.getName()).log(Level.INFO,
                    "Node " + nodeName + " removed", "Node removed");
        }
    }

    /**
     * Search for an element name, attribute name or text content or value
     */
    private void search() {
        // get selected node, if no node is selected, take the root node
        XMLTreeNode lspc = (XMLTreeNode) tree.getLastSelectedPathComponent();
        if (lspc == null) {
            lspc = rootNode;
        }

        // loop through all nodes
        Enumeration e = lspc.preorderEnumeration();
        while (e.hasMoreElements()) {
            XMLTreeNode nextElement = (XMLTreeNode) e.nextElement();
            Node element = (Node) nextElement.getUserObject();
            String tagName = element.getNodeName();

            // set all nodes to not found
            if (nextElement.getFound() == true) {
                nextElement.setFound(false);
            }

            // if text or attribute node check if content matches
            if ((nextElement instanceof TextElementTreeNode || nextElement instanceof AttributeTreeNode)
                    && element.getTextContent().contains(tasp.getSearchTextField().getText())
                    && !"E".equals(tasp.getSearchModeButton().getText())) {
                nextElement.setFound(true);
                setSelectedTreeNode(nextElement);
                continue;
            }

            // check if element name matches
            if (tagName.contains(tasp.getSearchTextField().getText())
                    && !"T".equals(tasp.getSearchModeButton().getText())) {
                nextElement.setFound(true);
                setSelectedTreeNode(nextElement);
            }
        }
    }

    /* BUTTON LISTENER DECLARATIONS */
    ActionListener editListener = (ActionEvent e) -> {
        edit(null);
    };

    ActionListener addListener = (ActionEvent e) -> {
        add();
    };

    ActionListener deleteListener = (ActionEvent e) -> {
        delete();
    };

    /**
     * Custom TreeSelectionListener to control the behaviour of the editing
     * buttons
     */
    private class CustomTreeSelectionListener implements TreeSelectionListener {

        @Override
        public void valueChanged(TreeSelectionEvent e) {
            // get currently selected tree node
            XMLTreeNode lastSelectedPathComponent = (XMLTreeNode) tree.getLastSelectedPathComponent();

            // check if user saved the content
            if (oldSelection != null
                    && (oldSelection instanceof TextElementTreeNode
                    || oldSelection instanceof AttributeTreeNode)) {
                Node node = (Node) oldSelection.getUserObject();

                String text;
                if (isEnum) {
                    text = list.getSelectedValue().toString();
                } else {
                    text = textArea.getText();
                }

                if (!text.equals(node.getTextContent())) {
                    int showConfirmDialog = JOptionPane.showConfirmDialog(rootPane, SAVE_CHANGE_TEXT,
                            SAVE_CHANGE_TITLE, JOptionPane.YES_NO_OPTION);
                    if (showConfirmDialog == JOptionPane.YES_OPTION) {
                        edit(oldSelection);
                    }
                }
            }

            // save the last selection for content editted checks
            oldSelection = lastSelectedPathComponent;

            // check if a node is selected
            if (lastSelectedPathComponent != null) {
                Node node = (Node) lastSelectedPathComponent.getUserObject();
                // set the current node in the XML
                xr.setCurrent(node);

                // check if node has content
                if (lastSelectedPathComponent instanceof TextElementTreeNode
                        || lastSelectedPathComponent instanceof AttributeTreeNode) {
                    // set text area with value or content
                    textArea.setText(node.getTextContent());
                    descLabel.setText(CONTENT_OF_TEXT + "'" + node.getNodeName() + "':");
                    // enable edit button
                    editButton.setEnabled(true);
                    // enable address choice button
                    chooseURIButton.setEnabled(true);
                    // set the addable nodes to null
                    addableChildNodes = null;
                    // disable the add button
                    addButton.setEnabled(false);
                    // check if enumeration
                    List<String> enumerations = xr.getEnum();
                    if (enumerations != null && enumerations.size() > 0) {
                        isEnum = true;
                        list = new JList();
                        DefaultListModel<String> listModel = new DefaultListModel<>();
                        list.setModel(listModel);

                        enumerations.stream().forEach((enumeration) -> {
                            listModel.addElement(enumeration);
                            if (enumeration.equals(node.getTextContent())) {
                                list.setSelectedValue(enumeration, true);
                            }
                        });
                        // show enumeration as list
                        textAreaScrollPane.setViewportView(list);
                    } else {
                        // tell that not an enum
                        isEnum = false;
                        // if no enumeration show text area
                        textAreaScrollPane.setViewportView(textArea);
                    }
                    // if no content (element node)
                } else if (lastSelectedPathComponent instanceof ElementTreeNode) {
                    // clear the text area
                    textArea.setText("");
                    descLabel.setText("Element '" + node.getNodeName() + "'. " + NO_CONTENT_ELEMENT_TEXT);
                    // disable edit button
                    editButton.setEnabled(false);
                    // disable address choice button
                    chooseURIButton.setEnabled(false);
                    // check if child nodes can be added
                    addableChildNodes = xr.getAddableChildNodes();
                    // enable the add button if there are addable nodes
                    addButton.setEnabled(addableChildNodes.size() > 0);
                    // set green panel visible
                    JPanel noTextPanel = new JPanel();
                    noTextPanel.setBackground(new Color(1, 115, 18));
                    textAreaScrollPane.setViewportView(noTextPanel);
                }

                // in case tree node represents an attribute
                if (lastSelectedPathComponent instanceof AttributeTreeNode) {
                    AttrImpl attr = (AttrImpl) node;
                    // enable delete button if attribute is removable
                    deleteButton.setEnabled(xr.isRemovableAttribute(attr.getOwnerElement(), attr.getName()));
                    // otherwise
                } else {
                    // enable delete button if element is removable
                    deleteButton.setEnabled(xr.isRemovableElement(((Element) node)));
                    // check if there are addable attributes
                    addableAttributes = xr.getAddableAttributes();
                    // in case the add button is not already enabled
                    if (!addButton.isEnabled()) {
                        // enable it if there are addable attributes
                        addButton.setEnabled(addableAttributes.size() > 0);
                    }
                }
                // if no tree node is selected
            } else {
                // clear text area and status label
                textArea.setText("");
                descLabel.setText(" ");

                // disable all editing button
                addButton.setEnabled(false);
                deleteButton.setEnabled(false);
                editButton.setEnabled(false);
            }
        }
    }

    /* TREE NODE DECLARATIONS */
    private static class XMLTreeNode extends DefaultMutableTreeNode {

        private boolean found = false;

        public XMLTreeNode(Object userObject) {
            super(userObject);
        }

        public void setFound(boolean found) {
            this.found = found;
        }

        public boolean getFound() {
            return found;
        }
    }

    private static class ElementTreeNode extends XMLTreeNode {

        public ElementTreeNode(Object userObject) {
            super(userObject);
        }
    }

    private static class TextElementTreeNode extends XMLTreeNode {

        public TextElementTreeNode(Object userObject) {
            super(userObject);
        }
    }

    private static class AttributeTreeNode extends XMLTreeNode {

        public AttributeTreeNode(Object userObject) {
            super(userObject);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        splitPane = new javax.swing.JSplitPane();
        rightPanel = new javax.swing.JPanel();
        textAreaScrollPane = new javax.swing.JScrollPane();
        textArea = new javax.swing.JTextArea();
        descLabel = new javax.swing.JLabel();
        editButton = new javax.swing.JButton();
        deleteButton = new javax.swing.JButton();
        addButton = new javax.swing.JButton();
        chooseURIButton = new javax.swing.JButton();
        greenPanel = new javax.swing.JPanel();
        menuBar = new javax.swing.JMenuBar();
        fileMenu = new javax.swing.JMenu();
        newXML = new javax.swing.JMenuItem();
        openExistingXML = new javax.swing.JMenuItem();
        jSeparator3 = new javax.swing.JPopupMenu.Separator();
        saveXML = new javax.swing.JMenuItem();
        saveXMLas = new javax.swing.JMenuItem();
        editMenu = new javax.swing.JMenu();
        undo = new javax.swing.JMenuItem();
        jSeparator2 = new javax.swing.JPopupMenu.Separator();
        validate = new javax.swing.JMenuItem();
        jSeparator4 = new javax.swing.JPopupMenu.Separator();
        insertTags = new javax.swing.JMenu();
        boldTag = new javax.swing.JMenuItem();
        italicTag = new javax.swing.JMenuItem();
        lineBreakTag = new javax.swing.JMenuItem();
        resourcesMenu = new javax.swing.JMenu();
        upload = new javax.swing.JMenuItem();
        download = new javax.swing.JMenuItem();
        helpMenu = new javax.swing.JMenu();
        howMenuItem = new javax.swing.JMenuItem();
        whoMenuItem = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setTitle("XML Based WebSite Accesser");

        textAreaScrollPane.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

        textArea.setColumns(20);
        textArea.setLineWrap(true);
        textArea.setRows(5);
        textArea.setWrapStyleWord(true);
        textAreaScrollPane.setViewportView(textArea);

        descLabel.setText(" ");

        editButton.setText("Ändan");
        editButton.setToolTipText("'n text vom Element ändan");
        editButton.setEnabled(false);

        deleteButton.setText("Leschn");
        deleteButton.setToolTipText("'s ausgwöhlte Element leschn");
        deleteButton.setEnabled(false);

        addButton.setText("Dazuatuan");
        addButton.setToolTipText("A Untaelement dazuatuan");
        addButton.setEnabled(false);

        chooseURIButton.setText("Lodn");
        chooseURIButton.setToolTipText("Die Adress von ana Datää lodn und eifügn");
        chooseURIButton.setEnabled(false);
        chooseURIButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chooseURIButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout rightPanelLayout = new javax.swing.GroupLayout(rightPanel);
        rightPanel.setLayout(rightPanelLayout);
        rightPanelLayout.setHorizontalGroup(
            rightPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(rightPanelLayout.createSequentialGroup()
                .addComponent(editButton, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(deleteButton, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(addButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(chooseURIButton, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(235, Short.MAX_VALUE))
            .addComponent(textAreaScrollPane, javax.swing.GroupLayout.Alignment.TRAILING)
            .addComponent(descLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        rightPanelLayout.setVerticalGroup(
            rightPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(rightPanelLayout.createSequentialGroup()
                .addComponent(descLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(textAreaScrollPane, javax.swing.GroupLayout.DEFAULT_SIZE, 298, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(rightPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(editButton)
                    .addComponent(deleteButton)
                    .addComponent(addButton)
                    .addComponent(chooseURIButton))
                .addContainerGap())
        );

        splitPane.setRightComponent(rightPanel);

        greenPanel.setBackground(new java.awt.Color(1, 115, 18));

        javax.swing.GroupLayout greenPanelLayout = new javax.swing.GroupLayout(greenPanel);
        greenPanel.setLayout(greenPanelLayout);
        greenPanelLayout.setHorizontalGroup(
            greenPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        greenPanelLayout.setVerticalGroup(
            greenPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 379, Short.MAX_VALUE)
        );

        splitPane.setLeftComponent(greenPanel);

        fileMenu.setText("Datää");

        newXML.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_N, java.awt.event.InputEvent.CTRL_MASK));
        newXML.setText("A neix...");
        newXML.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                newXMLActionPerformed(evt);
            }
        });
        fileMenu.add(newXML);

        openExistingXML.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_O, java.awt.event.InputEvent.CTRL_MASK));
        openExistingXML.setText("Öffnan...");
        openExistingXML.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                openExistingXMLActionPerformed(evt);
            }
        });
        fileMenu.add(openExistingXML);
        fileMenu.add(jSeparator3);

        saveXML.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_S, java.awt.event.InputEvent.CTRL_MASK));
        saveXML.setText("Ospeichan");
        saveXML.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveXMLActionPerformed(evt);
            }
        });
        fileMenu.add(saveXML);

        saveXMLas.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_U, java.awt.event.InputEvent.CTRL_MASK));
        saveXMLas.setText("Ospeichan unta...");
        saveXMLas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveXMLasActionPerformed(evt);
            }
        });
        fileMenu.add(saveXMLas);

        menuBar.add(fileMenu);

        editMenu.setText("Ummagschaftln");

        undo.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_Z, java.awt.event.InputEvent.CTRL_MASK));
        undo.setText("Zruck");
        undo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                undoActionPerformed(evt);
            }
        });
        editMenu.add(undo);
        editMenu.add(jSeparator2);

        validate.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_P, java.awt.event.InputEvent.CTRL_MASK));
        validate.setText("Validian");
        validate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                validateActionPerformed(evt);
            }
        });
        editMenu.add(validate);
        editMenu.add(jSeparator4);

        insertTags.setText("Eifügn");

        boldTag.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F, java.awt.event.InputEvent.ALT_MASK));
        boldTag.setText("Fejtt");
        boldTag.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                boldTagActionPerformed(evt);
            }
        });
        insertTags.add(boldTag);

        italicTag.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_K, java.awt.event.InputEvent.ALT_MASK));
        italicTag.setText("Kursiv");
        italicTag.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                italicTagActionPerformed(evt);
            }
        });
        insertTags.add(italicTag);

        lineBreakTag.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_U, java.awt.event.InputEvent.ALT_MASK));
        lineBreakTag.setText("Zalnumbruch");
        lineBreakTag.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                lineBreakTagActionPerformed(evt);
            }
        });
        insertTags.add(lineBreakTag);

        editMenu.add(insertTags);

        menuBar.add(editMenu);

        resourcesMenu.setText("Ressourcen");

        upload.setText("Datää(n) aufn Server lodn...");
        upload.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                uploadActionPerformed(evt);
            }
        });
        resourcesMenu.add(upload);

        download.setText("Datää vom Server owilodn...");
        download.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                downloadActionPerformed(evt);
            }
        });
        resourcesMenu.add(download);

        menuBar.add(resourcesMenu);

        helpMenu.setText("Hüf");

        howMenuItem.setText("Wia geht des?");
        howMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                howMenuItemActionPerformed(evt);
            }
        });
        helpMenu.add(howMenuItem);

        whoMenuItem.setText("Wer hot des gmocht?");
        whoMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                whoMenuItemActionPerformed(evt);
            }
        });
        helpMenu.add(whoMenuItem);

        menuBar.add(helpMenu);

        setJMenuBar(menuBar);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(splitPane)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(splitPane)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * Connect to server and choose a file the path of which is copied into the
     * text area
     */
    private void chooseURI() {
        // check that server path and login data are given
        if (serverPath != null && loginData != null) {
            try {
                // create new server dialog in choose mode
                ServerDULDialog scd = new ServerDULDialog(this, true, ServerDULDialog.CHOOSE_MODE);
                // set login data so that no new login possible
                scd.setLoginData(loginData);
                // start connection and directory listing
                scd.connectAndListDirs();
                // show Dialog and retrieve the chosen URI as String
                Object uriString = scd.showDialog();
                // if URI retrieved is String
                if (uriString != null) {
                    // add URI to text area
                    textArea.setText((String) uriString);
                    // log successful choice
                    Logger.getLogger(WebXMLAccesser.class.getName()).log(Level.INFO, "Chose URI: "
                            + (String) uriString, "URI chosen");
                    // if not a String
                } else {
                    // log that cancelled
                    Logger.getLogger(WebXMLAccesser.class.getName()).log(Level.INFO, "Choosing URI cancelled", "URI chosen");
                }
                // report error to log and user
            } catch (IOException ex) {
                JOptionPane.showMessageDialog(rootPane, ERROR_TEXT + ex, CANNOT_CHOOSE_TEXT, JOptionPane.ERROR_MESSAGE);
                Logger.getLogger(WebXMLAccesser.class.getName()).log(Level.SEVERE, null, ex);
            }
            // tell user that not connected
        } else {
            JOptionPane.showMessageDialog(rootPane, SERVER_ONLY_TEXT,
                    NO_SERVER_CONNECTION_TITLE, JOptionPane.INFORMATION_MESSAGE);
        }
    }

    private void chooseURIButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chooseURIButtonActionPerformed
        // TODO add your handling code here:        
        chooseURI();
    }//GEN-LAST:event_chooseURIButtonActionPerformed

    /**
     * Previous actions class to hold undo information
     */
    private class PreviousActions {

        private final ArrayList<OldElement> previousActions = new ArrayList<>();
        private final int maxRecords;

        public PreviousActions(int maxRecords) {
            this.maxRecords = maxRecords;
        }

        public class OldElement {

            public OldElement() {
            }
        }

        public class EditedContent extends OldElement {

            public String textContent;
            public XMLTreeNode treeNode;

            public EditedContent(String textContent, XMLTreeNode treeNode) {
                this.textContent = textContent;
                this.treeNode = treeNode;
            }
        }

        public class AddedElement extends OldElement {

            public XMLTreeNode treeNode;

            public AddedElement(XMLTreeNode treeNode) {
                this.treeNode = treeNode;
            }
        }

        public class AddedAttribute extends OldElement {

            public XMLTreeNode treeNode;

            public AddedAttribute(XMLTreeNode treeNode) {
                this.treeNode = treeNode;
            }
        }

        public class DeletedElement extends OldElement {

            public Node parentNode;
            public Node deletedNode;
            public Node siblingNode;
            public XMLTreeNode parentTreeNode;
            public XMLTreeNode treeNode;
            public int index;

            public DeletedElement(Node parentNode, Node deletedNode, Node siblingNode,
                    XMLTreeNode parentTreeNode, XMLTreeNode treeNode, int index) {
                this.parentNode = parentNode;
                this.deletedNode = deletedNode;
                this.siblingNode = siblingNode;
                this.parentTreeNode = parentTreeNode;
                this.treeNode = treeNode;
                this.index = index;
            }
        }

        public class DeletedAttribute extends OldElement {

            public Element ownerElement;
            public AttrImpl attribute;
            public int index;
            public XMLTreeNode parentTreeNode;
            public XMLTreeNode treeNode;

            public DeletedAttribute(Element ownerElement, AttrImpl attribute,
                    int index, XMLTreeNode parentTreeNode, XMLTreeNode treeNode) {
                this.ownerElement = ownerElement;
                this.attribute = attribute;
                this.index = index;
                this.parentTreeNode = parentTreeNode;
                this.treeNode = treeNode;
            }
        }

        public void add(OldElement oe) {

            previousActions.add(oe);
            if (previousActions.size() == maxRecords) {
                previousActions.remove(0);
            }
        }

        public OldElement getLast() {
            return previousActions.get(previousActions.size() - 1);
        }

        public void deleteLast() {
            previousActions.remove(previousActions.size() - 1);
        }

        public boolean hasRecords() {
            return previousActions.size() > 0;
        }
    }

    /**
     * Read in the last undo data and go back to the old state
     */
    private void undo() {
        // check if there is something to undo
        if (undoList.hasRecords()) {
            // get last undo entry
            PreviousActions.OldElement last = undoList.getLast();
            // set the old selection to null so there won't be any check
            oldSelection = null;
            // in case an element or attribute was editted
            if (last instanceof PreviousActions.EditedContent) {
                // cast to editted content
                PreviousActions.EditedContent edCon = (PreviousActions.EditedContent) last;
                // get the tree node and the node held by it
                XMLTreeNode treeNode = (XMLTreeNode) edCon.treeNode;
                Node node = (Node) treeNode.getUserObject();
                // set node value/content and text area to old content
                node.setTextContent(edCon.textContent);
                textArea.setText(edCon.textContent);
                // reload the tree node and select the undone node
                model.reload(treeNode.getParent());
                setSelectedTreeNode(treeNode);
                // log change
                Logger.getLogger(WebXMLAccesser.class
                        .getName()).log(Level.INFO,
                                "Undo edit of node: " + node.getNodeName(), "Undo");
            }
            // in case an element was added
            if (last instanceof PreviousActions.AddedElement) {
                // cast to added element
                PreviousActions.AddedElement addEl = (PreviousActions.AddedElement) last;
                // get the added tree node and the node held
                XMLTreeNode addedTreeNode = (XMLTreeNode) addEl.treeNode;
                Node addedNode = (Node) addedTreeNode.getUserObject();
                // get the parent tree node and the node held
                XMLTreeNode parentTreeNode = (XMLTreeNode) addedTreeNode.getParent();
                Node parentNode = (Node) parentTreeNode.getUserObject();
                // remove the node from the XML and the tree model
                parentNode.removeChild(addedNode);
                model.removeNodeFromParent(addedTreeNode);
                // set selection to the parent tree node
                setSelectedTreeNode(parentTreeNode);
                // log removal
                Logger.getLogger(WebXMLAccesser.class
                        .getName()).log(Level.INFO, "Undo add of element "
                                + addedNode.getNodeName(), "Undo");
            }
            // in case an attribute was added
            if (last instanceof PreviousActions.AddedAttribute) {
                // cast to added attribute
                PreviousActions.AddedAttribute addAt = (PreviousActions.AddedAttribute) last;
                // get the attribute
                AttrImpl attr = (AttrImpl) addAt.treeNode.getUserObject();
                // get the parent tree node
                XMLTreeNode parentTreeNode = (XMLTreeNode) addAt.treeNode.getParent();
                // get the attribute's owner element
                Element ownerElement = attr.getOwnerElement();
                // remove the attribute from th XML
                ownerElement.removeAttribute(attr.getName());
                // remove the attribute node from the tree
                model.removeNodeFromParent(addAt.treeNode);
                // det the selection to the parent node
                setSelectedTreeNode(parentTreeNode);
                // log removal
                Logger.getLogger(WebXMLAccesser.class
                        .getName()).log(Level.INFO, "Undo add of attribute "
                                + attr.getName(), "Undo");
            }
            // in case an element was deleted
            if (last instanceof PreviousActions.DeletedElement) {
                // cast to deleted element
                PreviousActions.DeletedElement delEl = (PreviousActions.DeletedElement) last;
                // get the former parent element
                Element parentElement = (Element) delEl.parentNode;
                // get the former next sibling
                Node nextSibling = delEl.siblingNode;
                // get the removed node
                Node node = delEl.deletedNode;
                // check where to reestablish in the XML
                if (nextSibling != null) {
                    parentElement.insertBefore(node, nextSibling);
                } else {
                    parentElement.appendChild(node);
                }
                // get the former parent tree node
                XMLTreeNode parentTreeNode = (XMLTreeNode) delEl.parentTreeNode;
                // insert the old tree node at its former position
                parentTreeNode.insert(delEl.treeNode, delEl.index);
                // reload the the parent node
                model.reload(parentTreeNode);
                // set the selection to the reestablished tree node
                setSelectedTreeNode((XMLTreeNode) delEl.treeNode);
                // log readd
                Logger.getLogger(WebXMLAccesser.class
                        .getName()).log(Level.INFO, "Undo delete of element "
                                + node.getNodeName(), "Undo");
            }
            // in case an attribute was removed
            if (last instanceof PreviousActions.DeletedAttribute) {
                // cast to removed attribute
                PreviousActions.DeletedAttribute delAt = (PreviousActions.DeletedAttribute) last;
                // reestablish the removed attribute
                delAt.ownerElement.setAttributeNodeNS(delAt.attribute);
                // reestablish the removed tree node
                delAt.parentTreeNode.insert(delAt.treeNode, delAt.index);
                // reload the parent node
                model.reload(delAt.parentTreeNode);
                // set selection to the restablished tree node
                setSelectedTreeNode((XMLTreeNode) delAt.treeNode);
                // log readd
                Logger.getLogger(WebXMLAccesser.class
                        .getName()).log(Level.INFO, "Undo delete of Attribute "
                                + delAt.attribute.getNodeName(), "Undo");
            }
            // delete the undo entry
            undoList.deleteLast();
        }
    }

    private void undoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_undoActionPerformed
        // TODO add your handling code here:
        undo();
    }//GEN-LAST:event_undoActionPerformed
    /**
     * Validate the whole XML document
     */
    private void validateDocument() {
        // check if a XMLReader was loaded
        if (xr != null) {
            // validate and retrieve the error messages
            String validationError = xr.validateDocument();
            // if there are error messages
            if (validationError != null) {
                // load a list dialog and show errors
                ListDialog ld = new ListDialog(this, true);
                String text = INVALID_TEXT + validationError;
                ld.getTextArea().setText(text);
                ld.setTitle(INVALID_TITLE);
                ld.setVisible(true);
                // log validation erors
                Logger.getLogger(WebXMLAccesser.class
                        .getName()).log(Level.INFO, "Validation error:\n"
                                + validationError, "Validation");
                // if there are no errors
            } else {
                JOptionPane.showMessageDialog(rootPane, VALID_TEXT,
                        VALID_TITLE, JOptionPane.INFORMATION_MESSAGE);
                // log that validationErrors
                Logger.getLogger(WebXMLAccesser.class
                        .getName()).log(Level.INFO, "Is valid", "Validation");
            }
        }
    }
    private void validateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_validateActionPerformed
        // TODO add your handling code here:
        validateDocument();
    }//GEN-LAST:event_validateActionPerformed

    private void newXMLActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_newXMLActionPerformed
        // TODO add your handling code here:
        newXML();
    }//GEN-LAST:event_newXMLActionPerformed

    /**
     * ask the user for opening a XML Schema file
     *
     * @return the XML Schema file or null if the user cancelled
     */
    private File openXSD() {

        JFileChooser fc = new JFileChooser();
        FileNameExtensionFilter filter = new FileNameExtensionFilter(XSD_EXTENSION_TEXT, XSD_EXTENSION_TYPE);
        fc.setFileFilter(filter);
        fc.setDialogTitle(OPEN_XSD_FILE_TITLE);
        int returnVal = fc.showOpenDialog(this);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            return fc.getSelectedFile();
        }
        return null;
    }

    /**
     * Delete any temporal files and directory
     *
     * @throws IOException
     */
    private void clearTemp() throws IOException {
        File tempDir = new File(MainFrame.TEMP_PATH);
        if (tempDir.exists()) {
            FileUtils.deleteDirectory(tempDir);
        }
    }

    /**
     * Set to default left panel and everything default
     */
    private void setGreenPanel() {
        // set possible old files and server connection information to null
        chosenFile = null;
        serverPath = null;
        loginData = null;
        // set tree to null
        model = null;
        tree = null;
        // add the default green panel
        splitPane.setTopComponent(greenPanel);
        splitPane.setDividerLocation(this.getWidth() / 3);
        // set the XML reader to null
        xr = null;
    }

    /**
     * Create a new XML document
     */
    private void newXML() {
        // set default yes option
        int showConfirmDialog = JOptionPane.YES_OPTION;
        // check if there is already an XMLReader open
        if (xr != null) {
            // retrieve the user choice
            showConfirmDialog = JOptionPane.showConfirmDialog(rootPane, WANT_NEW_TEXT,
                    WANT_NEW_TITLE, JOptionPane.YES_NO_OPTION);
        }
        // check if the user wants to open a new XML
        if (showConfirmDialog == JOptionPane.YES_OPTION) {

            // set to default
            setGreenPanel();

            // prompt the user to open a new XML Schema file
            File xsdFile = openXSD();
            // check if a XML Schema file was chosen and loaded
            if (xsdFile != null) {
                try {
                    // create a new XSD reader representing the schema
                    XSDReader xsdr = XSDReader.instantiate(xsdFile);
                    // check if creating the schema was successful
                    if (xsdr != null) {
                        // if yes create a new default XML document
                        Document basicXML = xsdr.createBasicXMLFromXSD(0);
                        // create a new XMLReader with that default XML
                        xr = XMLReader.instantiateFromObject(xsdr, basicXML);
                        // if successful
                        if (xr != null) {
                            // load a tree representation of the XML
                            loadTree();
                            // clear any temporary information
                            clearTemp();
                            // log successful creation of the new XML document
                            Logger.getLogger(WebXMLAccesser.class
                                    .getName()).log(Level.INFO, "New XML created", "New XML");
                            // if an error occurred while loading the XML
                        } else {
                            // tell the user
                            JOptionPane.showMessageDialog(rootPane, ERROR_LOAD_XML_TEXT,
                                    ERROR_TITLE, JOptionPane.ERROR_MESSAGE);
                            // clear any objects created and go back to default state
                            setGreenPanel();
                            // and log it
                            Logger.getLogger(WebXMLAccesser.class
                                    .getName()).log(Level.SEVERE, "New XML creation failed", "New XML");
                        }
                        // if the XML Schema is invalid or another loading error occurred
                    } else {
                        // tell the user
                        JOptionPane.showMessageDialog(rootPane, ERROR_LOAD_XSD_TEXT,
                                ERROR_TITLE, JOptionPane.ERROR_MESSAGE);
                        // clear any objects created and go back to default state
                        setGreenPanel();
                        // and log it
                        Logger.getLogger(WebXMLAccesser.class
                                .getName()).log(Level.SEVERE, "New XML creation failed due to XSD", "New XML");
                    }
                    // if an error occurred while loading and creation
                } catch (ClassNotFoundException | InstantiationException |
                        IllegalAccessException | SAXException |
                        IOException | ParserConfigurationException | NullPointerException ex) {
                    // tell the user
                    ListDialog ld = new ListDialog(this, true);
                    ld.getTextArea().setText(ERROR_TEXT + ex);
                    ld.setTitle(ERROR_NEW_TITLE);
                    ld.setVisible(true);
                    // clear any objects created and go back to default state
                    setGreenPanel();
                    // and log the error
                    Logger.getLogger(WebXMLAccesser.class
                            .getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    private void openExistingXMLActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_openExistingXMLActionPerformed
        // TODO add your handling code here:
        openXML();
    }//GEN-LAST:event_openExistingXMLActionPerformed

    /**
     * Open a XML
     */
    private void openXML() {
        // prompt the user to open a new XML Schema file
        File xsdFile = openXSD();
        // check if the XML Schema file was loaded successfully
        if (xsdFile != null) {
            // let the user choose from where to get the XML
            String[] options = {FROM_SERVER_OPTION, FROM_LOCAL_OPTION, CANCEL_OPTION};
            // show the choose source dialog
            int showOptionDialog = JOptionPane.showOptionDialog(rootPane, CHOOSE_SOURCE_TEXT,
                    CHOOSE_SOURCE_TITLE, JOptionPane.DEFAULT_OPTION, JOptionPane.QUESTION_MESSAGE,
                    null, options, options[0]);
            // do according to choice
            switch (showOptionDialog) {
                // if cancelled, log
                case 2:
                    Logger.getLogger(WebXMLAccesser.class
                            .getName()).log(Level.INFO, "Open XML cancelled", "Open XML");
                    return;
                // if from local
                case 1:
                    // let the user choose a XML file
                    JFileChooser fc = new JFileChooser();
                    fc.setDialogTitle(OPEN_XML_FILE_TITLE);
                    FileNameExtensionFilter filter = new FileNameExtensionFilter(XML_EXTENSION_TEXT, XML_EXTENSION_TYPE);
                    fc.setFileFilter(filter);
                    int showOptionDialogXML = fc.showOpenDialog(this);
                    // if the user chose a file
                    if (showOptionDialogXML == JFileChooser.APPROVE_OPTION) {
                        // set to default
                        setGreenPanel();
                        // set the chosen file to the XML file
                        chosenFile = fc.getSelectedFile();
                        try {
                            // create a XML document from the file
                            Document xmlDoc = XMLReader.makeXMLDoc(chosenFile);
                            // create a new XMLReader from schema file and xml document
                            xr = XMLReader.instantiateFromFile(xsdFile, xmlDoc);
                            // check if successful
                            if (xr != null) {
                                // load the tree representing the XML
                                loadTree();
                                // set any server connection information to null
                                serverPath = null;
                                loginData = null;
                                // clear any temporarily saved files
                                clearTemp();
                                // log opening of local file
                                Logger.getLogger(WebXMLAccesser.class
                                        .getName()).log(Level.INFO, "Opened XML from local file", "Open XML");
                                // if creation of the XMLReader was not successful
                            } else {
                                // clear any objects created and go back to default state
                                setGreenPanel();
                                // and tell the user
                                JOptionPane.showMessageDialog(rootPane, ERROR_LOAD_XML_OR_XSD_TEXT,
                                        ERROR_TITLE, JOptionPane.ERROR_MESSAGE);
                                // log opening of local file
                                Logger.getLogger(WebXMLAccesser.class
                                        .getName()).log(Level.INFO, "Opening XML from local file failed", "Open XML");
                            }
                            // in case an error occurred
                        } catch (ParserConfigurationException | SAXException | IOException | ClassNotFoundException | InstantiationException | IllegalAccessException | NullPointerException ex) {
                            // clear any objects created and go back to default state
                            setGreenPanel();
                            // log it
                            Logger.getLogger(WebXMLAccesser.class
                                    .getName()).log(Level.SEVERE, null, ex);
                            // tell the user
                            ListDialog ld = new ListDialog(this, true);
                            ld.getTextArea().setText(ERROR_TEXT + ex);
                            ld.setTitle(OPEN_XML_ERROR_TITLE);
                            ld.setVisible(true);
                        }
                        // if cancelled
                    } else {
                        // log it
                        Logger.getLogger(WebXMLAccesser.class
                                .getName()).log(Level.INFO, "Open XML cancelled", "Open XML");
                    }
                    break;
                // if from server
                case 0:
                    try {
                        // start a new server dialog and get the response
                        ServerDULDialog sdd = new ServerDULDialog(this, true, ServerDULDialog.DOWNLOAD_MODE);
                        Object response = sdd.showDialog();
                        if (response != null) {
                            // set to default
                            setGreenPanel();

                            // get the server dialog response and set the information
                            ServerDownload sd = (ServerDownload) response;
                            chosenFile = sd.getFile();
                            serverPath = sd.getServerPath();
                            loginData = sd.getLoginData();
                            // make a local savety copy
                            Files.copy(chosenFile.toPath().toAbsolutePath(), 
                                    Paths.get(WebXMLAccesser.getHomeDirectory() 
                                            + "/COPY_OF_" + chosenFile.getName()),
                                    StandardCopyOption.REPLACE_EXISTING);
                            // create a XML document from the local file
                            Document xmlDoc = XMLReader.makeXMLDoc(chosenFile);
                            // create a XMLReader holding the XML
                            xr = XMLReader.instantiateFromFile(xsdFile, xmlDoc);
                            // if successful
                            if (xr != null) {
                                // load the tree representing the XML
                                loadTree();
                                // and log it
                                Logger.getLogger(WebXMLAccesser.class
                                        .getName()).log(Level.INFO, "Opened XML from Server", "Open XML");
                                // if not successful
                            } else {
                                // tell the user
                                JOptionPane.showMessageDialog(rootPane, ERROR_LOAD_XML_OR_XSD_TEXT,
                                        ERROR_TITLE, JOptionPane.ERROR_MESSAGE);
                                // log it
                                Logger.getLogger(WebXMLAccesser.class
                                        .getName()).log(Level.INFO, "Opening XML from Server failed", "Open XML");
                                // delete the downloaded file
                                if (chosenFile != null) {
                                    try {
                                        Files.deleteIfExists(chosenFile.toPath());
                                    } catch (IOException ex1) {
                                        Logger.getLogger(WebXMLAccesser.class
                                                .getName()).log(Level.SEVERE, null, ex1);
                                    }
                                }
                                // clear any objects created and go back to default state
                                setGreenPanel();
                            }
                            // if cancelled
                        } else {
                            // log it
                            Logger.getLogger(WebXMLAccesser.class
                                    .getName()).log(Level.INFO, "Open XML from Server Cancelled or Error", "Open XML");
                        }
                        // if an error occurred
                    } catch (IOException | ParserConfigurationException |
                            SAXException | ClassNotFoundException |
                            InstantiationException | IllegalAccessException | NullPointerException ex) {
                        // log it
                        Logger.getLogger(WebXMLAccesser.class
                                .getName()).log(Level.SEVERE, null, ex);
                        // tell the user
                        ListDialog ld = new ListDialog(this, true);
                        ld.getTextArea().setText(ERROR_TEXT + ex);
                        ld.setTitle(OPEN_XML_ERROR_TITLE);
                        ld.setVisible(true);
                        // and delete the downloaded file
                        if (chosenFile != null) {
                            try {
                                Files.deleteIfExists(chosenFile.toPath());
                            } catch (IOException ex1) {
                                Logger.getLogger(WebXMLAccesser.class
                                        .getName()).log(Level.SEVERE, null, ex1);
                            }
                        }
                        // clear any objects created and go back to default state
                        setGreenPanel();
                    }
                    break;
            }
        }
    }

    private void boldTagActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_boldTagActionPerformed
        // TODO add your handling code here:
        textArea.append(BOLD_TEXT);
    }//GEN-LAST:event_boldTagActionPerformed

    private void italicTagActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_italicTagActionPerformed
        // TODO add your handling code here:
        textArea.append(ITALICS_TEXT);
    }//GEN-LAST:event_italicTagActionPerformed

    /**
     * Upload a file to the server
     */
    private void upload() {
        // create a file chooser and prompt the user to select local files
        JFileChooser fc = new JFileChooser();
        fc.setMultiSelectionEnabled(true);
        int showOpenDialog = fc.showOpenDialog(this);
        // if the user chose a file
        if (showOpenDialog == JFileChooser.APPROVE_OPTION) {

            try {
                // start a new server dialog a predefine the selected file
                ServerDULDialog sud = new ServerDULDialog(this, true, ServerDULDialog.UPLOAD_MODE);
                sud.setChosenFiles(fc.getSelectedFiles());
                // connect to the server if a server connection is defined
                if (loginData != null) {
                    sud.setLoginData(loginData);
                    sud.connectAndListDirs();
                }
                // retreive server response
                Object showDialog = sud.showDialog();

                if (showDialog != null) {
                    // if successful log
                    if (((boolean) showDialog) == true) {
                        Logger.getLogger(WebXMLAccesser.class
                                .getName()).log(Level.INFO, "Uploaded File", "Upload File");
                        // if not log too
                    } else {
                        Logger.getLogger(WebXMLAccesser.class
                                .getName()).log(Level.INFO, "Uploading File Failed",
                                        "Upload File");
                    }
                } else {
                    // log it
                    Logger.getLogger(WebXMLAccesser.class
                            .getName()).log(Level.INFO, "Upload XML cancelled or error", "Open XML");
                }
                // in case an error occurred
            } catch (IOException ex) {
                // tell the user
                JOptionPane.showMessageDialog(rootPane, UPLOAD_ERROR_TEXT + ex,
                        ERROR_TITLE, JOptionPane.ERROR_MESSAGE);
                // and log
                Logger.getLogger(WebXMLAccesser.class
                        .getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    private void uploadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_uploadActionPerformed
        // TODO add your handling code here:        
        upload();
    }//GEN-LAST:event_uploadActionPerformed

    /**
     * Save a XML locally
     */
    private void saveToLocalFile() {
        try {
            // print the XML to the chosen file path
            xr.printDocument(null, chosenFile.getAbsolutePath());
            // log saving
            Logger.getLogger(WebXMLAccesser.class
                    .getName()).log(Level.INFO, "XML saved to "
                            + chosenFile.getAbsolutePath(), "Save XML");
            // if an error occurred
        } catch (TransformerException | IOException ex) {
            // log it
            Logger.getLogger(WebXMLAccesser.class
                    .getName()).log(Level.SEVERE, null, ex);
            // and tell the user
            JOptionPane.showMessageDialog(this,
                    SAVE_ERROR_TEXT + ex, ERROR_TITLE, JOptionPane.ERROR_MESSAGE);
        }
    }

    /**
     * Save the XML
     */
    private void saveXML() {
        // check if XMLReader is not null
        if (xr != null) {
            // validate the document
            String validationErrors = xr.validateDocument();
            // if no errors
            if (validationErrors == null) {
                // redirect to save to if no chosen file specified
                if (chosenFile == null) {
                    saveXMLas();
                    return;
                }
                // if the XML is from server
                if (serverPath != null && loginData != null) {
                    try {
                        // print the altered XML to the local file
                        xr.printDocument(null, chosenFile.getAbsolutePath());
                        // create a new progress dialog and upload the altered XML
                        ProgressDialog pd = new ProgressDialog(this, true, chosenFile,
                                serverPath, loginData);
                        boolean showDialog = pd.showDialog();
                        // if upload not successful
                        if (!showDialog) {
                            // the the user
                            JOptionPane.showMessageDialog(this, NO_UPLOAD_TEXT,
                                    ERROR_TITLE, JOptionPane.ERROR_MESSAGE);
                            // log that not saved
                            Logger.getLogger(WebXMLAccesser.class
                                    .getName()).log(Level.SEVERE, "XML not saved", "Save XML");

                        }
                        // if an error occurred
                    } catch (IOException | TransformerException ex) {
                        // log it
                        Logger.getLogger(WebXMLAccesser.class
                                .getName()).log(Level.SEVERE, null, ex);
                        // and tell the user
                        JOptionPane.showMessageDialog(this, UPLOAD_ERROR_TEXT
                                + ex, ERROR_TITLE, JOptionPane.ERROR_MESSAGE);
                    }
                    // if local saving
                } else {
                    // save locally
                    saveToLocalFile();
                }
                // if not validationErrors
            } else {
                // tell the user
                ListDialog ld = new ListDialog(this, true);
                ld.getTextArea().setText(INVALID_TEXT + validationErrors);
                ld.setTitle(INVALID_TITLE);
                ld.setVisible(true);
                // and log it
                Logger.getLogger(WebXMLAccesser.class
                        .getName()).log(Level.INFO, "XML not saved, not valid", "Save XML");
            }
        }
    }

    private void saveXMLActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveXMLActionPerformed
        // TODO add your handling code here:      
        saveXML();
    }//GEN-LAST:event_saveXMLActionPerformed

    /**
     * Save XML with new title locally
     */
    private void saveXMLas() {
        // check if XMLReader is not null
        if (xr != null) {
            // validate the document
            String validationErrors = xr.validateDocument();
            // if no errors
            if (validationErrors == null) {
                // ceate a file chooser and let the user choose a file location
                JFileChooser fc = new JFileChooser();
                int showSaveDialog = fc.showSaveDialog(this);
                if (showSaveDialog == JFileChooser.APPROVE_OPTION) {
                    // set the chosen file to the selected file
                    chosenFile = fc.getSelectedFile();
                    // and save i t locally
                    saveToLocalFile();
                }
                // if the XML is not valid
            } else {
                // tell the user
                ListDialog ld = new ListDialog(this, true);
                ld.getTextArea().setText(INVALID_TEXT + validationErrors);
                ld.setTitle(INVALID_TITLE);
                ld.setVisible(true);
                // and log it
                Logger.getLogger(WebXMLAccesser.class
                        .getName()).log(Level.INFO, "XML not saved, not valid", "Save XML");
            }
        }
    }

    private void saveXMLasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveXMLasActionPerformed
        // TODO add your handling code here:
        saveXMLas();
    }//GEN-LAST:event_saveXMLasActionPerformed

    private void lineBreakTagActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_lineBreakTagActionPerformed
        // TODO add your handling code here:
        textArea.append(NEW_LINE_TEXT);
    }//GEN-LAST:event_lineBreakTagActionPerformed

    private void whoMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_whoMenuItemActionPerformed
        // TODO add your handling code here:
        String about = this.getClass().getPackage().getImplementationTitle() + " " + this.getClass().getPackage().getImplementationVersion();
        JOptionPane.showMessageDialog(rootPane, "DA KOUTSCH HOT'S GMOCHT! © 2016\n" + about,
                "Wo's herkummt", JOptionPane.INFORMATION_MESSAGE,
                new ImageIcon(getClass().getResource("/webxmlaccesser/Icon.png")));
    }//GEN-LAST:event_whoMenuItemActionPerformed

    private void howMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_howMenuItemActionPerformed
        // TODO add your handling code here:
        HelpFrame hf = new HelpFrame();
        hf.setVisible(true);
    }//GEN-LAST:event_howMenuItemActionPerformed

    /**
     * Download file from server
     */
    private void download() {
        try {
            // start a server dialog in free download mode
            ServerDULDialog sud = new ServerDULDialog(this, true, ServerDULDialog.FREE_DOWNLOAD_MODE);
            Object showDialog = sud.showDialog();
            // if download was not cancelled
            if (showDialog != null) {
                // log
                Logger.getLogger(WebXMLAccesser.class
                        .getName()).log(Level.INFO,
                                "Downloaded File " + ((ServerDownload) showDialog).getFile().getName(), "File Download");
                // otherwise log too
            } else {
                // log it
                Logger.getLogger(WebXMLAccesser.class
                        .getName()).log(Level.INFO, "Open XML from Server Cancelled or Error", "Open XML");
            }
            // if an error occurred
        } catch (IOException ex) {
            // tell the user
            JOptionPane.showMessageDialog(rootPane, DOWNLOAD_ERROR_TEXT + ex,
                    ERROR_TITLE, JOptionPane.ERROR_MESSAGE);
            // and log
            Logger.getLogger(WebXMLAccesser.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
    }
    private void downloadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_downloadActionPerformed
        download();
    }//GEN-LAST:event_downloadActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton addButton;
    private javax.swing.JMenuItem boldTag;
    private javax.swing.JButton chooseURIButton;
    private javax.swing.JButton deleteButton;
    private javax.swing.JLabel descLabel;
    private javax.swing.JMenuItem download;
    private javax.swing.JButton editButton;
    private javax.swing.JMenu editMenu;
    private javax.swing.JMenu fileMenu;
    private javax.swing.JPanel greenPanel;
    private javax.swing.JMenu helpMenu;
    private javax.swing.JMenuItem howMenuItem;
    private javax.swing.JMenu insertTags;
    private javax.swing.JMenuItem italicTag;
    private javax.swing.JPopupMenu.Separator jSeparator2;
    private javax.swing.JPopupMenu.Separator jSeparator3;
    private javax.swing.JPopupMenu.Separator jSeparator4;
    private javax.swing.JMenuItem lineBreakTag;
    private javax.swing.JMenuBar menuBar;
    private javax.swing.JMenuItem newXML;
    private javax.swing.JMenuItem openExistingXML;
    private javax.swing.JMenu resourcesMenu;
    private javax.swing.JPanel rightPanel;
    private javax.swing.JMenuItem saveXML;
    private javax.swing.JMenuItem saveXMLas;
    private javax.swing.JSplitPane splitPane;
    private javax.swing.JTextArea textArea;
    private javax.swing.JScrollPane textAreaScrollPane;
    private javax.swing.JMenuItem undo;
    private javax.swing.JMenuItem upload;
    private javax.swing.JMenuItem validate;
    private javax.swing.JMenuItem whoMenuItem;
    // End of variables declaration//GEN-END:variables
}
