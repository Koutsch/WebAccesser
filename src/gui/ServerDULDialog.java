/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import dataObjects.FTPDirectory;
import java.awt.Component;
import java.beans.PropertyChangeEvent;
import java.io.File;
import java.io.IOException;
import java.util.concurrent.ExecutionException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultListModel;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JTree;
import javax.swing.ListCellRenderer;
import javax.swing.SwingWorker;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeSelectionModel;
import org.apache.commons.net.ftp.FTPFile;
import webxmlaccesser.FTPConnector;
import dataObjects.ServerDownload;
import dataObjects.ServerLoginData;
import java.awt.Color;
import java.awt.GridLayout;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.concurrent.CancellationException;
import javax.swing.JFileChooser;
import javax.swing.JScrollPane;
import javax.swing.tree.DefaultTreeCellRenderer;
import webxmlaccesser.WebXMLAccesser;

/**
 *
 * @author koutsch
 */
public class ServerDULDialog extends javax.swing.JDialog {

    private final DefaultListModel<FTPFile> listModel = new DefaultListModel<>();

    private final FTPConnector ftpc;
    private FTPFile chosenFTPFile;
    private File[] chosenFiles;
    private String currentWorkingDirectory;
    private ServerLoginData loginData;
    private Object response;

    private JTree tree;
    private final JScrollPane leftScrollPane;
    private JScrollPane rightScrollPane;
    private JList<FTPFile> list;

    /**
     * If in download mode the user can to choose the connection but the local
     * file is already given
     */
    public static final int DOWNLOAD_MODE = 0;

    /**
     * If in upload mode it is used to upload a file
     */
    public static final int UPLOAD_MODE = 1;

    /**
     * If in choose mode a file path can be obtained from a file on the server
     */
    public static final int CHOOSE_MODE = 2;

    /**
     * If in free download mode the user can choose the local path oneself
     */
    public static final int FREE_DOWNLOAD_MODE = 3;

    private final int mode;
    private ConnectWorker cw;
    private FTPConnector.DownloadWorker dw;
    private FTPConnector.UploadWorker uw;

    boolean connectionEnabled = true;

    private final String DOWNLOAD_BTN_TEXT = "Owilodn";
    private final String DOWNLOAD_TITLE = "A Datää owilodn";
    private final String CHOOSE_BTN_TEXT = "Auswöhln";
    private final String CHOOSE_TITLE = "A Datää auswöhln";
    private final String UPLOAD_BTN_TEXT = "Aufilodn";
    private final String UPLOAD_TITLE = "A Datää aufilodn";
    private final String LOAD_STATUS_TEXT = "Server-Datään werdn glodn!";
    private final String CONNECT_ERROR_TEXT = "Hob mi net vabindn kinnan. Föülla:\n";
    private final String CONNECT_ERROR_TITLE = "Vabindung hot net hinghaut";
    private final String DOWNLOAD_ERROR_TEXT = "Hob's net owilodn kinnan. Föülla:\n";
    private final String DOWNLOAD_ERROR_TITLE = "Föülla ban Owilodn";
    private final String UPLOAD_ERROR_TEXT = "Hob's net aufilodn kinnan. Föülla:\n";
    private final String UPLOAD_ERROR_TITLE = "Föülla ban Aufilodn";
    private final String NO_CHOICE_TEXT = "Wöhl die Datää aus wost owilodn wüllst!";
    private final String NO_CHOICE_TITLE = "Nix ausgwöhlt";
    private final String NO_CONN_INFO_TEXT = "I glaub du host wos vagessn! Schau nou amol!";
    private final String NO_CONN_INFO_TITLE = "Vagessn ausfülln";

    /**
     * Creates new form ServerDLDialog
     *
     * @param parent the parent of this dialog
     * @param modal set if modal
     * @param mode the mode to be set to
     * @throws java.io.IOException
     */
    public ServerDULDialog(java.awt.Frame parent, boolean modal, int mode) throws IOException {
        // set fields
        super(parent, modal);
        this.ftpc = new FTPConnector();
        this.mode = mode;

        // initiate predefined contents
        initComponents();
        // set sart in middle of screen
        this.setLocationRelativeTo(null);
        // set the main panel layout
        mainPanel.setLayout(new GridLayout(1, 2));
        // add a scroll pane for the directory tree
        leftScrollPane = new JScrollPane();
        mainPanel.add(leftScrollPane);
        // if not in upload mode
        if (mode != UPLOAD_MODE) {
            // add a list for the files
            list = new JList<>();
            list.setModel(listModel);
            // put list into scroll pane
            rightScrollPane = new JScrollPane(list);
            list.setCellRenderer(new CustomListCellRenderer());
            mainPanel.add(rightScrollPane);
            // set title and buttons according to mode
            if (mode == DOWNLOAD_MODE || mode == FREE_DOWNLOAD_MODE) {
                chooseButton.setText(DOWNLOAD_BTN_TEXT);
                this.setTitle(DOWNLOAD_TITLE);
            }

            if (mode == CHOOSE_MODE) {
                chooseButton.setText(CHOOSE_BTN_TEXT);
                this.setTitle(CHOOSE_TITLE);
            }
            // if upload mode just set texts
        } else {
            chooseButton.setText(UPLOAD_BTN_TEXT);
            this.setTitle(UPLOAD_TITLE);
        }
    }

    /**
     *
     * @param c
     */
    @Override
    public final void setLocationRelativeTo(Component c) {
        super.setLocationRelativeTo(c);
    }

    /**
     * Show the dialog and return the result on dispose
     *
     * @return a response object dependent on the mode
     */
    public Object showDialog() {
        setVisible(true);
        return response;
    }

    /**
     * Set the chosen local file to work with
     *
     * @param chosenFiles a file array representing the chosen file
     */
    public void setChosenFiles(File[] chosenFiles) {
        this.chosenFiles = chosenFiles;
    }

    /**
     * set the login data to work with and disable the connection fields and
     * button
     *
     * @param loginData a LoginData object representing the login parameters for
     * a FTP connection
     */
    public void setLoginData(ServerLoginData loginData) {
        this.loginData = loginData;
        connectionEnabled = false;
        connectEnabled(false);
    }

    /**
     * Enable or disable the connection fields and button
     *
     * @param enabled if true they are enabled, otherwise disabled
     */
    public void connectEnabled(boolean enabled) {
        addressTextField.setEnabled(enabled);
        portTextField.setEnabled(enabled);
        userTextField.setEnabled(enabled);
        passwordField.setEnabled(enabled);
        connectButton.setEnabled(enabled);
    }

    /**
     * A SwingWorker to connect to a FTP server
     */
    private class ConnectWorker extends SwingWorker<Object, Void> {

        FTPConnector ftpc;

        @Override
        protected Object doInBackground() throws InterruptedException, IOException {

            failIfInterrupted();

            // create a new FTPConnector
            ftpc = new FTPConnector();
            // connect to FTP server
            boolean connectAndLogin = ftpc.connectAndLogin(loginData);

            failIfInterrupted();

            // if connected
            if (connectAndLogin) {

                failIfInterrupted();

                // loop recursively through directories and fill tree
                DefaultMutableTreeNode rootNode = fillDirs();
                // log connection type
                Logger.getLogger(WebXMLAccesser.class.getName()).log(Level.INFO, "FTP System Type: "
                        + ftpc.getSystemType(), "Remote Type");
                // disconnect 
                ftpc.disconnect();
                // return the root treeNode for the tree
                return rootNode;
                // if no connection possible
            } else {
                failIfInterrupted();

                // diconnect
                ftpc.disconnect();
                // return server reply string
                return ftpc.getReplyString();
            }
        }

        /**
         * loop through the directories and files of then server and fill a tree
         * for representation
         *
         * @return a DefaultMutableTreeNode representing the file system
         * @throws IOException
         * @throws InterruptedException
         */
        private DefaultMutableTreeNode fillDirs() throws IOException, InterruptedException {

            failIfInterrupted();

            // prepare the file list
            ArrayList<FTPFile> files = new ArrayList<>();
            // create a new FTPDirectory and add directory and files contained
            FTPDirectory ftpd = new FTPDirectory(ftpc.printWorkingDirectory(), files);
            // create a new tree treeNode
            DefaultMutableTreeNode treeNode = new DefaultMutableTreeNode(ftpd);

            // get the files and subdirectories of a file
            FTPFile[] listFiles = ftpc.listFiles();
            // loop through the directory
            for (FTPFile file : listFiles) {
                failIfInterrupted();

                // if an accessable file add it to the file list
                if (file.isFile()
                        && file.hasPermission(FTPFile.USER_ACCESS, FTPFile.WRITE_PERMISSION)) {
                    files.add(file);
                }

                // if an accessable directory except . and ..
                if (file.isDirectory()
                        && file.hasPermission(FTPFile.USER_ACCESS, FTPFile.WRITE_PERMISSION)
                        && !file.getName().startsWith(".")) {

                    // go to the subdirectory
                    ftpc.changeWorkingDirectory(file.getName());
                    // log directory change
                    Logger.getLogger(WebXMLAccesser.class.getName()).log(Level.INFO,
                            "Changed directory to " + ftpc.printWorkingDirectory(), "FTP Directory");
                    // create subdirectories recursively
                    DefaultMutableTreeNode childDir = fillDirs();
                    // when finished go back to the current directory
                    ftpc.changeToParentDirectory();
                    // and the child tree treeNode to the parent
                    treeNode.add(childDir);
                }
            }
            // return the tree treeNode
            return treeNode;
        }

        private void failIfInterrupted() throws InterruptedException, IOException {
            if (Thread.currentThread().isInterrupted()) {
                ftpc.disconnect();
                throw new InterruptedException("Interrupted while searching");
            }
        }
    }

    /**
     * A custom cell renderer for displaying the directory names in the tree
     */
    public class CustomTreeCellRenderer extends DefaultTreeCellRenderer {

        /**
         * No border when selected
         *
         * @return null
         */
        @Override
        public Color getBorderSelectionColor() {
            return null;
        }

        /**
         * display the right content in the tree
         *
         * @param tree
         * @param value
         * @param sel
         * @param exp
         * @param leaf
         * @param row
         * @param hasFocus
         * @return
         */
        @Override
        public Component getTreeCellRendererComponent(JTree tree, Object value,
                boolean sel, boolean exp, boolean leaf, int row, boolean hasFocus) {

            // get the current tree node
            DefaultMutableTreeNode treeNode = (DefaultMutableTreeNode) value;
            // get the FTPDirectory held by the tree node
            FTPDirectory directory = (FTPDirectory) treeNode.getUserObject();
            // get the directory name
            String name = directory.getName();
            // get the name of the file
            Path path = Paths.get(name).getFileName();

            // set the directory name to the path name of "Server" if it's the root (the server)
            if (path != null) {
                name = path.toString();
            } else {
                name = "Server";
            }
            // set the cell renderer component
            Component treeCellRendererComponent
                    = super.getTreeCellRendererComponent(tree, name, sel, exp, leaf, row, hasFocus);
            // return it
            return treeCellRendererComponent;
        }
    }

    /**
     * A custom cell renderer for displaying the file names in the list
     */
    private class CustomListCellRenderer extends JLabel implements ListCellRenderer {

        @Override
        public Component getListCellRendererComponent(JList jlist, Object value, int index, boolean isSelected, boolean cellHasFocus) {
            setOpaque(true);
            if (isSelected) {

                setBackground(jlist.getSelectionBackground());
                setForeground(jlist.getSelectionForeground());
            } else {
                setBackground(jlist.getBackground());
                setForeground(jlist.getForeground());
            }

            FTPFile ftpf = (FTPFile) value;
            setText(ftpf.getName());

            return this;
        }
    }

    /**
     * A custom tree selection listener for showing the right files on click on
     * a directory in the tree
     */
    private class CustomTreeSelectionListener implements TreeSelectionListener {

        @Override
        public void valueChanged(TreeSelectionEvent tse) {
            DefaultMutableTreeNode lastSelectedPathComponent = (DefaultMutableTreeNode) tree.getLastSelectedPathComponent();
            // if a tree node is selected
            if (lastSelectedPathComponent != null) {
                // display the files
                FTPDirectory ftpd = (FTPDirectory) lastSelectedPathComponent.getUserObject();
                if (mode != UPLOAD_MODE) {
                    listModel.removeAllElements();
                    ArrayList<FTPFile> fileNames = ftpd.getFiles();
                    if (!fileNames.isEmpty()) {
                        fileNames.stream().forEach((fileName) -> {
                            listModel.addElement(fileName);
                        });
                    }
                }
                // and change the current directory path to that directory
                currentWorkingDirectory = ftpd.getName();
            }
        }
    }

    /**
     * connect to the server and list the directories
     */
    public void connectAndListDirs() {

        // disable the connector fields and button
        connectEnabled(false);
        // set the status label to inform the user that a dierectory list is created
        statusLabel.setText(LOAD_STATUS_TEXT);
        // create a new connection worker
        cw = new ConnectWorker();
        // add logic for success
        cw.addPropertyChangeListener((PropertyChangeEvent pce) -> {
            switch (pce.getPropertyName()) {
                case "state":
                    switch ((SwingWorker.StateValue) pce.getNewValue()) {
                        case DONE: {
                            try {
                                // get the worker response
                                Object getObj = cw.get();
                                // if a tree node was returned
                                if (getObj instanceof DefaultMutableTreeNode) {
                                    // cast the response to a tree node
                                    DefaultMutableTreeNode rootNode = (DefaultMutableTreeNode) getObj;
                                    // set the tree
                                    DefaultTreeModel model = new DefaultTreeModel(rootNode);
                                    tree = new JTree(model);
                                    tree.setCellRenderer(new CustomTreeCellRenderer());
                                    tree.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
                                    tree.addTreeSelectionListener(new CustomTreeSelectionListener());
                                    // add the tree to the scroll pane
                                    leftScrollPane.setViewportView(tree);
                                    // enable the choose button
                                    chooseButton.setEnabled(true);
                                    // enable the connection buttons if in free connection mode
                                    ServerDULDialog.this.connectEnabled(connectionEnabled);
                                    // reset the status label
                                    statusLabel.setText(" ");
                                    // if connection fails
                                } else {
                                    // log the reason
                                    Logger.getLogger(WebXMLAccesser.class.getName()).log(Level.SEVERE, "Connection not possible: "
                                            + getObj, "FTP Connect");
                                    // let the user know
                                    JOptionPane.showMessageDialog(rootPane, CONNECT_ERROR_TEXT + getObj,
                                            CONNECT_ERROR_TITLE, JOptionPane.ERROR_MESSAGE);
                                    // check if the dialog should be further displayed
                                    if (connectionEnabled) {
                                        // enable the connection buttons again if in free connection mode
                                        ServerDULDialog.this.connectEnabled(true);
                                        // disable the choose button again
                                        chooseButton.setEnabled(false);
                                    } else {
                                        ServerDULDialog.this.dispose();
                                    }
                                }
                                // if the worker was interrupted or disturbed in execution
                            } catch (InterruptedException | ExecutionException ex) {
                                // log the error
                                Logger.getLogger(WebXMLAccesser.class.getName()).log(Level.SEVERE, null, ex);
                                // let the user know
                                JOptionPane.showMessageDialog(rootPane, CONNECT_ERROR_TEXT + ex,
                                        CONNECT_ERROR_TITLE, JOptionPane.ERROR_MESSAGE);
                                // check if the dialog should be further displayed
                                if (connectionEnabled) {
                                    // enable the connection buttons again if in free connection mode
                                    ServerDULDialog.this.connectEnabled(true);
                                    // disable the choose button again
                                    chooseButton.setEnabled(false);
                                } else {
                                    ServerDULDialog.this.dispose();
                                }
                                // if cancelled
                            } catch (CancellationException ex) {
                                // log it
                                Logger.getLogger(WebXMLAccesser.class.getName()).log(Level.SEVERE, null, ex);
                                // check if the dialog should be further displayed
                                if (connectionEnabled) {
                                    // enable the connection buttons again if in free connection mode
                                    ServerDULDialog.this.connectEnabled(true);
                                    // disable the choose button again
                                    chooseButton.setEnabled(false);
                                } else {
                                    ServerDULDialog.this.dispose();
                                }
                            }
                        }
                    }
            }
        });
        // do the connection
        cw.execute();
    }

    /**
     * Download a file
     *
     * @throws IOException
     * @throws SecurityException
     */
    private void downloadFile() throws IOException, SecurityException {

        // get the selected file
        Object selectedValue = list.getSelectedValue();
        // check if selection was made
        if (selectedValue != null) {
            // set chosen file to selected
            chosenFTPFile = (FTPFile) selectedValue;
            // disable connection fields, button and choose button
            connectEnabled(false);
            chooseButton.setEnabled(false);
            // disable closing
            this.setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);

            // set the local copy location
            String path;
            switch (mode) {
                case DOWNLOAD_MODE:
                default:
                    // copy to the temporary path
                    File tempDir = new File(MainFrame.TEMP_PATH);
                    if (!tempDir.exists()) {
                        tempDir.mkdir();
                    }
                    path = tempDir.getAbsolutePath();
                    break;
                case FREE_DOWNLOAD_MODE:
                    // let the user choose
                    JFileChooser fc = new JFileChooser();
                    fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
                    fc.setDialogTitle("Wöcha Ordna deaf's sei?");
                    int returnVal = fc.showOpenDialog(this);
                    if (returnVal == JFileChooser.APPROVE_OPTION) {
                        path = fc.getSelectedFile().getAbsolutePath();
                    } else {
                        // put connection fields, button and choose button to old state
                        connectEnabled(connectionEnabled);
                        chooseButton.setEnabled(true);
                        // stop downloading
                        return;
                    }
                    break;
            }

            // start a download worker
            dw = ftpc.new DownloadWorker(currentWorkingDirectory + "/" + chosenFTPFile.getName(),
                    path + "/" + chosenFTPFile.getName(), loginData, chosenFTPFile.getSize());
            dw.addPropertyChangeListener((PropertyChangeEvent pce) -> {
                switch (pce.getPropertyName()) {
                    case "progress":
                        // show progress in the progress bar
                        progressBar.setValue((Integer) pce.getNewValue());
                        break;
                    case "state":
                        switch ((SwingWorker.StateValue) pce.getNewValue()) {
                            // when finished downloading
                            case DONE: {
                                try {
                                    // set response to a ServerDownload containing the file and connection data
                                    response = new ServerDownload(dw.get(), currentWorkingDirectory, loginData);
                                    // in case of an error or interruption
                                } catch (InterruptedException | ExecutionException ex) {
                                    // log it
                                    Logger.getLogger(WebXMLAccesser.class.getName()).log(Level.SEVERE, null, ex);
                                    // let the user know
                                    JOptionPane.showMessageDialog(rootPane, DOWNLOAD_ERROR_TEXT,
                                            DOWNLOAD_ERROR_TITLE, JOptionPane.ERROR_MESSAGE);
                                    // and set response to null
                                    response = null;
                                    // when cancelled log it
                                } catch (CancellationException ex) {
                                    Logger.getLogger(WebXMLAccesser.class.getName()).log(Level.SEVERE, null, ex);
                                    response = null;
                                }
                                // close the dialog
                                ServerDULDialog.this.setVisible(false);
                                ServerDULDialog.this.dispose();
                            }
                        }
                }
            });
            // do the download
            dw.execute();
            // tell the user if nothing chosen
        } else {
            JOptionPane.showMessageDialog(rootPane, NO_CHOICE_TEXT,
                    NO_CHOICE_TITLE, JOptionPane.WARNING_MESSAGE);
        }
    }

    /**
     * Upload a file
     *
     * @throws IOException
     */
    private void uploadFile() throws IOException {

        // disable connection fields, button and choose button
        connectEnabled(false);
        chooseButton.setEnabled(false);
        // disable closing
        this.setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);

        // start a upload worker
        uw = ftpc.new UploadWorker(currentWorkingDirectory, chosenFiles, loginData);
        uw.addPropertyChangeListener((PropertyChangeEvent pce) -> {
            switch (pce.getPropertyName()) {
                case "progress":
                    // show progress in the progress bar
                    progressBar.setValue((Integer) pce.getNewValue());
                    break;
                case "state":
                    switch ((SwingWorker.StateValue) pce.getNewValue()) {
                        // when finished uploading
                        case DONE: {
                            try {
                                // prepare result
                                response = uw.get();
                            } catch (InterruptedException | ExecutionException ex) {
                                // log it
                                Logger.getLogger(WebXMLAccesser.class.getName()).log(Level.SEVERE, null, ex);
                                // let the user know
                                JOptionPane.showMessageDialog(rootPane, UPLOAD_ERROR_TEXT + ex,
                                        UPLOAD_ERROR_TITLE, JOptionPane.ERROR_MESSAGE);
                                // and set response to null
                                response = null;
                                // when cancelled log it
                            } catch (CancellationException ex) {
                                Logger.getLogger(WebXMLAccesser.class.getName()).log(Level.SEVERE, null, ex);
                                response = null;
                            }
                            // close the dialog
                            ServerDULDialog.this.setVisible(false);
                            ServerDULDialog.this.dispose();
                        }
                    }
            }
        });
        // do the upload
        uw.execute();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        addressTextField = new javax.swing.JTextField();
        portTextField = new javax.swing.JTextField();
        userTextField = new javax.swing.JTextField();
        passwordField = new javax.swing.JPasswordField();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        connectButton = new javax.swing.JButton();
        chooseButton = new javax.swing.JButton();
        cancelButton = new javax.swing.JButton();
        progressBar = new javax.swing.JProgressBar();
        jSeparator1 = new javax.swing.JSeparator();
        mainPanel = new javax.swing.JPanel();
        statusLabel = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Aufi- und owilodn");

        jLabel1.setText("Adress");

        jLabel2.setText("Port");

        jLabel3.setText("Benutza");

        jLabel4.setText("Passwuat");

        connectButton.setText("Vabindn!");
        connectButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                connectButtonActionPerformed(evt);
            }
        });

        chooseButton.setText("Lodn");
        chooseButton.setEnabled(false);
        chooseButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chooseButtonActionPerformed(evt);
            }
        });

        cancelButton.setText("Obbrechn");
        cancelButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout mainPanelLayout = new javax.swing.GroupLayout(mainPanel);
        mainPanel.setLayout(mainPanelLayout);
        mainPanelLayout.setHorizontalGroup(
            mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        mainPanelLayout.setVerticalGroup(
            mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 209, Short.MAX_VALUE)
        );

        statusLabel.setText(" ");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jSeparator1)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(chooseButton, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(cancelButton, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(statusLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(addressTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel1))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel2)
                                    .addComponent(portTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel3)
                                    .addComponent(userTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(passwordField, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(connectButton))
                                    .addComponent(jLabel4))
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addComponent(mainPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(progressBar, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addContainerGap())))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel1)
                        .addComponent(jLabel2))
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel3)
                        .addComponent(jLabel4)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(addressTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(portTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(userTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(passwordField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(connectButton))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(mainPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(chooseButton)
                    .addComponent(cancelButton)
                    .addComponent(statusLabel))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(progressBar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void cancelButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cancelButtonActionPerformed

        // cancel all workers if active
        if (cw != null) {
            cw.cancel(true);
        }

        if (dw != null) {
            dw.cancel(true);
        }

        if (uw != null) {
            uw.cancel(true);
        }

        // set the response to null
        response = null;

        // close the dialog
        this.setVisible(false);
        this.dispose();
    }//GEN-LAST:event_cancelButtonActionPerformed

    /**
     * Perform a connection
     */
    private void connect() {
        // prepare the login data
        String pw = new String(passwordField.getPassword());
        if (!"".equals(addressTextField.getText())
                && !"".equals(userTextField.getText())
                && !"".equals(pw)) {

            int portNmbr;
            if ("".equals(portTextField.getText())) {
                // if no port given, set to -1 to ignore it
                portNmbr = -1;
            } else {
                portNmbr = Integer.parseInt(portTextField.getText());
            }
            // set the login data
            loginData = new ServerLoginData(addressTextField.getText(),
                    portNmbr, userTextField.getText(), pw);
            // connect and list the directories and files on the server
            connectAndListDirs();
            // tell the user if forgot information
        } else {
            JOptionPane.showMessageDialog(rootPane, NO_CONN_INFO_TEXT,
                    NO_CONN_INFO_TITLE, JOptionPane.WARNING_MESSAGE);
        }
    }

    private void connectButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_connectButtonActionPerformed

        connect();
    }//GEN-LAST:event_connectButtonActionPerformed

    /**
     * Choose a file a down/upload it
     */
    private void choose() {
        switch (mode) {
            case DOWNLOAD_MODE:
            case FREE_DOWNLOAD_MODE:
                try {
                    downloadFile();
                } catch (IOException | SecurityException ex) {
                    JOptionPane.showMessageDialog(rootPane, DOWNLOAD_ERROR_TEXT + ex,
                            DOWNLOAD_ERROR_TITLE, JOptionPane.ERROR_MESSAGE);
                    Logger.getLogger(WebXMLAccesser.class.getName()).log(Level.SEVERE, null, ex);
                    // set the response to null
                    response = null;
                    // close the dialog
                    this.setVisible(false);
                    this.dispose();
                }
                break;
            case UPLOAD_MODE:
                try {
                    uploadFile();
                } catch (IOException | SecurityException ex) {
                    JOptionPane.showMessageDialog(rootPane, UPLOAD_ERROR_TEXT + ex,
                            UPLOAD_ERROR_TITLE, JOptionPane.ERROR_MESSAGE);
                    Logger.getLogger(WebXMLAccesser.class.getName()).log(Level.SEVERE, null, ex);
                    // set the response to null
                    response = null;
                    // close the dialog
                    this.setVisible(false);
                    this.dispose();
                }
                break;
            case CHOOSE_MODE:
                Object selectedValue = list.getSelectedValue();
                if (selectedValue != null) {
                    chosenFTPFile = (FTPFile) selectedValue;
                    response = currentWorkingDirectory + "/" + chosenFTPFile.getName();
                    this.setVisible(false);
                    this.dispose();
                } else {
                    JOptionPane.showMessageDialog(rootPane, NO_CHOICE_TEXT, 
                            NO_CHOICE_TITLE, JOptionPane.ERROR_MESSAGE);
                }
                break;
        }
    }

    private void chooseButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chooseButtonActionPerformed
        choose();
    }//GEN-LAST:event_chooseButtonActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField addressTextField;
    private javax.swing.JButton cancelButton;
    private javax.swing.JButton chooseButton;
    private javax.swing.JButton connectButton;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JPanel mainPanel;
    private javax.swing.JPasswordField passwordField;
    private javax.swing.JTextField portTextField;
    private javax.swing.JProgressBar progressBar;
    private javax.swing.JLabel statusLabel;
    private javax.swing.JTextField userTextField;
    // End of variables declaration//GEN-END:variables
}
